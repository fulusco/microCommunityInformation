package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govPartyMember.GovPartyMemberDto;
import com.java110.po.govPartyMember.GovPartyMemberPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovPartyMemberInnerServiceSMO
 * @Description 党员管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govPartyMemberApi")
public interface IGovPartyMemberInnerServiceSMO {


    @RequestMapping(value = "/saveGovPartyMember", method = RequestMethod.POST)
    public int saveGovPartyMember(@RequestBody GovPartyMemberPo govPartyMemberPo);

    @RequestMapping(value = "/updateGovPartyMember", method = RequestMethod.POST)
    public int updateGovPartyMember(@RequestBody  GovPartyMemberPo govPartyMemberPo);

    @RequestMapping(value = "/deleteGovPartyMember", method = RequestMethod.POST)
    public int deleteGovPartyMember(@RequestBody  GovPartyMemberPo govPartyMemberPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govPartyMemberDto 数据对象分享
     * @return GovPartyMemberDto 对象数据
     */
    @RequestMapping(value = "/queryGovPartyMembers", method = RequestMethod.POST)
    List<GovPartyMemberDto> queryGovPartyMembers(@RequestBody GovPartyMemberDto govPartyMemberDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govPartyMemberDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovPartyMembersCount", method = RequestMethod.POST)
    int queryGovPartyMembersCount(@RequestBody GovPartyMemberDto govPartyMemberDto);
}
