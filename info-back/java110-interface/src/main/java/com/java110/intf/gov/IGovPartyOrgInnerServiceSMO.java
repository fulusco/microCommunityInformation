package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govPartyOrg.GovPartyOrgDto;
import com.java110.po.govPartyOrg.GovPartyOrgPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovPartyOrgInnerServiceSMO
 * @Description 党组织接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govPartyOrgApi")
public interface IGovPartyOrgInnerServiceSMO {


    @RequestMapping(value = "/saveGovPartyOrg", method = RequestMethod.POST)
    public int saveGovPartyOrg(@RequestBody GovPartyOrgPo govPartyOrgPo);

    @RequestMapping(value = "/updateGovPartyOrg", method = RequestMethod.POST)
    public int updateGovPartyOrg(@RequestBody  GovPartyOrgPo govPartyOrgPo);

    @RequestMapping(value = "/deleteGovPartyOrg", method = RequestMethod.POST)
    public int deleteGovPartyOrg(@RequestBody  GovPartyOrgPo govPartyOrgPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govPartyOrgDto 数据对象分享
     * @return GovPartyOrgDto 对象数据
     */
    @RequestMapping(value = "/queryGovPartyOrgs", method = RequestMethod.POST)
    List<GovPartyOrgDto> queryGovPartyOrgs(@RequestBody GovPartyOrgDto govPartyOrgDto);


    @RequestMapping(value = "/queryNotGovPartyOrg", method = RequestMethod.POST)
    List<GovPartyOrgDto> queryNotGovPartyOrg(@RequestBody GovPartyOrgDto govPartyOrgDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govPartyOrgDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovPartyOrgsCount", method = RequestMethod.POST)
    int queryGovPartyOrgsCount(@RequestBody GovPartyOrgDto govPartyOrgDto);
}
