package com.java110.intf.assets;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govCommunity.GovCommunityDto;
import com.java110.po.govCommunity.GovCommunityPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovCommunityInnerServiceSMO
 * @Description 小区信息接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-assets", configuration = {FeignConfiguration.class})
@RequestMapping("/govCommunityApi")
public interface IGovCommunityInnerServiceSMO {


    @RequestMapping(value = "/saveGovCommunity", method = RequestMethod.POST)
    public int saveGovCommunity(@RequestBody GovCommunityPo govCommunityPo);

    @RequestMapping(value = "/updateGovCommunity", method = RequestMethod.POST)
    public int updateGovCommunity(@RequestBody  GovCommunityPo govCommunityPo);

    @RequestMapping(value = "/deleteGovCommunity", method = RequestMethod.POST)
    public int deleteGovCommunity(@RequestBody  GovCommunityPo govCommunityPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govCommunityDto 数据对象分享
     * @return GovCommunityDto 对象数据
     */
    @RequestMapping(value = "/queryGovCommunitys", method = RequestMethod.POST)
    List<GovCommunityDto> queryGovCommunitys(@RequestBody GovCommunityDto govCommunityDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govCommunityDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovCommunitysCount", method = RequestMethod.POST)
    int queryGovCommunitysCount(@RequestBody GovCommunityDto govCommunityDto);
}
