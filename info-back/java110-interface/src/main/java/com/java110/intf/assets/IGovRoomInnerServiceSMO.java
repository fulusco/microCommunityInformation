package com.java110.intf.assets;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govRoom.GovRoomDto;
import com.java110.po.govRoom.GovRoomPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovRoomInnerServiceSMO
 * @Description 房屋管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-assets", configuration = {FeignConfiguration.class})
@RequestMapping("/govRoomApi")
public interface IGovRoomInnerServiceSMO {


    @RequestMapping(value = "/saveGovRoom", method = RequestMethod.POST)
    public int saveGovRoom(@RequestBody GovRoomPo govRoomPo);

    @RequestMapping(value = "/updateGovRoom", method = RequestMethod.POST)
    public int updateGovRoom(@RequestBody  GovRoomPo govRoomPo);

    @RequestMapping(value = "/deleteGovRoom", method = RequestMethod.POST)
    public int deleteGovRoom(@RequestBody  GovRoomPo govRoomPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govRoomDto 数据对象分享
     * @return GovRoomDto 对象数据
     */
    @RequestMapping(value = "/queryGovRooms", method = RequestMethod.POST)
    List<GovRoomDto> queryGovRooms(@RequestBody GovRoomDto govRoomDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govRoomDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovRoomsCount", method = RequestMethod.POST)
    int queryGovRoomsCount(@RequestBody GovRoomDto govRoomDto);
}
