package com.java110.dto.govOwnerPerson;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 人口户籍关系数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovOwnerPersonDto extends PageDto implements Serializable {

    private String govPersonId;
private String caId;
private String govOpId;
private String govOwnerId;
private String relCd;


    private Date createTime;

    private String statusCd = "0";


    public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getGovOpId() {
        return govOpId;
    }
public void setGovOpId(String govOpId) {
        this.govOpId = govOpId;
    }
public String getGovOwnerId() {
        return govOwnerId;
    }
public void setGovOwnerId(String govOwnerId) {
        this.govOwnerId = govOwnerId;
    }
public String getRelCd() {
        return relCd;
    }
public void setRelCd(String relCd) {
        this.relCd = relCd;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    private String datasourceType = "999999";
    public String getDatasourceType() {
        return datasourceType;
    }

    public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
}
