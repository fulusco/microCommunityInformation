package com.java110.dto.govFloor;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 建筑物管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovFloorDto extends PageDto implements Serializable {

    private String layerCount;
private String govCommunityId;
private String communityName;
private String unitCount;
private String floorType;
private String typeName;
private String govFloorId;
private String floorIcon;
private String oldFloorIcon;
private String floorNum;
private String ramark;
private String floorUse;
private String personName;
private String caId;
private String caName;
private String floorName;
private String floorArea;
private String personLink;


    private Date createTime;

    private String statusCd = "0";


    public String getLayerCount() {
        return layerCount;
    }
public void setLayerCount(String layerCount) {
        this.layerCount = layerCount;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getUnitCount() {
        return unitCount;
    }
public void setUnitCount(String unitCount) {
        this.unitCount = unitCount;
    }
public String getFloorType() {
        return floorType;
    }
public void setFloorType(String floorType) {
        this.floorType = floorType;
    }
public String getGovFloorId() {
        return govFloorId;
    }
public void setGovFloorId(String govFloorId) {
        this.govFloorId = govFloorId;
    }
public String getFloorIcon() {
        return floorIcon;
    }
public void setFloorIcon(String floorIcon) {
        this.floorIcon = floorIcon;
    }
public String getFloorNum() {
        return floorNum;
    }
public void setFloorNum(String floorNum) {
        this.floorNum = floorNum;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getFloorUse() {
        return floorUse;
    }
public void setFloorUse(String floorUse) {
        this.floorUse = floorUse;
    }
public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getFloorName() {
        return floorName;
    }
public void setFloorName(String floorName) {
        this.floorName = floorName;
    }
public String getFloorArea() {
        return floorArea;
    }
public void setFloorArea(String floorArea) {
        this.floorArea = floorArea;
    }
public String getPersonLink() {
        return personLink;
    }
public void setPersonLink(String personLink) {
        this.personLink = personLink;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getCaName() {
        return caName;
    }

    public void setCaName(String caName) {
        this.caName = caName;
    }

    public String getOldFloorIcon() {
        return oldFloorIcon;
    }

    public void setOldFloorIcon(String oldFloorIcon) {
        this.oldFloorIcon = oldFloorIcon;
    }

    private String datasourceType = "999999";
    public String getDatasourceType() {
        return datasourceType;
    }

    public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
}
