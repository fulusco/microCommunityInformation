package com.java110.dto.govCommunity;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 小区信息数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovCommunityDto extends PageDto implements Serializable {

    private String personName;
private String communitySecure;
private String govCommunityId;
private String caId;
private String caName;
private String propertyType;
private String communityIcon;
private String oldCommunityIcon;
private String communityName;
private String managerName;
private String ramark;
private String personLink;
private String fileSaveName;


    private Date createTime;

    private String statusCd = "0";


    public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getCommunitySecure() {
        return communitySecure;
    }
public void setCommunitySecure(String communitySecure) {
        this.communitySecure = communitySecure;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getPropertyType() {
        return propertyType;
    }
public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }
public String getCommunityIcon() {
        return communityIcon;
    }
public void setCommunityIcon(String communityIcon) {
        this.communityIcon = communityIcon;
    }
public String getCommunityName() {
        return communityName;
    }
public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }
public String getManagerName() {
        return managerName;
    }
public void setManagerName(String managerName) {
        this.managerName = managerName;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getPersonLink() {
        return personLink;
    }
public void setPersonLink(String personLink) {
        this.personLink = personLink;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getCaName() {
        return caName;
    }

    public void setCaName(String caName) {
        this.caName = caName;
    }

    public String getFileSaveName() {
        return fileSaveName;
    }

    public void setFileSaveName(String fileSaveName) {
        this.fileSaveName = fileSaveName;
    }

    public String getOldCommunityIcon() {
        return oldCommunityIcon;
    }

    public void setOldCommunityIcon(String oldCommunityIcon) {
        this.oldCommunityIcon = oldCommunityIcon;
    }

    private String datasourceType = "999999";
    public String getDatasourceType() {
        return datasourceType;
    }

    public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
}
