package com.java110.dto.govRoom;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 房屋管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovRoomDto extends PageDto implements Serializable {

    private String roomRight;
private String isSettle;
private String govCommunityId;
private String ownerTel;
private String govFloorId;
private String roomArea;
private String isConservati;
private String govRoomId;
private String ownerId;
private String layer;
private String ramark;
private String roomNum;
private String ownerName;
private String roomAddress;
private String caId;
private String roomType;
private String caName;
private String communityName;
private String floorName;


    private Date createTime;

    private String statusCd = "0";


    public String getRoomRight() {
        return roomRight;
    }
public void setRoomRight(String roomRight) {
        this.roomRight = roomRight;
    }
public String getIsSettle() {
        return isSettle;
    }
public void setIsSettle(String isSettle) {
        this.isSettle = isSettle;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getOwnerTel() {
        return ownerTel;
    }
public void setOwnerTel(String ownerTel) {
        this.ownerTel = ownerTel;
    }
public String getGovFloorId() {
        return govFloorId;
    }
public void setGovFloorId(String govFloorId) {
        this.govFloorId = govFloorId;
    }
public String getRoomArea() {
        return roomArea;
    }
public void setRoomArea(String roomArea) {
        this.roomArea = roomArea;
    }
public String getIsConservati() {
        return isConservati;
    }
public void setIsConservati(String isConservati) {
        this.isConservati = isConservati;
    }
public String getGovRoomId() {
        return govRoomId;
    }
public void setGovRoomId(String govRoomId) {
        this.govRoomId = govRoomId;
    }
public String getOwnerId() {
        return ownerId;
    }
public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }
public String getLayer() {
        return layer;
    }
public void setLayer(String layer) {
        this.layer = layer;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getRoomNum() {
        return roomNum;
    }
public void setRoomNum(String roomNum) {
        this.roomNum = roomNum;
    }
public String getOwnerName() {
        return ownerName;
    }
public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
public String getRoomAddress() {
        return roomAddress;
    }
public void setRoomAddress(String roomAddress) {
        this.roomAddress = roomAddress;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getRoomType() {
        return roomType;
    }
public void setRoomType(String roomType) {
        this.roomType = roomType;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getCaName() {
        return caName;
    }

    public void setCaName(String caName) {
        this.caName = caName;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public String getFloorName() {
        return floorName;
    }

    public void setFloorName(String floorName) {
        this.floorName = floorName;
    }

    private String datasourceType = "999999";
    public String getDatasourceType() {
        return datasourceType;
    }

    public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
}
