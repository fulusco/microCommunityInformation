package com.java110.dto.govCommunityArea;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 区域管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovCommunityAreaDto extends PageDto implements Serializable {

    private String caName;
private String areaCode;
private String person;
private String caId;
private String caSpace;
private String caCode;
private String ramark;
private String caAddress;
private String personLink;


    private Date createTime;

    private String statusCd = "0";


    public String getCaName() {
        return caName;
    }
public void setCaName(String caName) {
        this.caName = caName;
    }
public String getAreaCode() {
        return areaCode;
    }
public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }
public String getPerson() {
        return person;
    }
public void setPerson(String person) {
        this.person = person;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getCaSpace() {
        return caSpace;
    }
public void setCaSpace(String caSpace) {
        this.caSpace = caSpace;
    }
public String getCaCode() {
        return caCode;
    }
public void setCaCode(String caCode) {
        this.caCode = caCode;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getCaAddress() {
        return caAddress;
    }
public void setCaAddress(String caAddress) {
        this.caAddress = caAddress;
    }
public String getPersonLink() {
        return personLink;
    }
public void setPersonLink(String personLink) {
        this.personLink = personLink;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    private String datasourceType = "999999";
    public String getDatasourceType() {
        return datasourceType;
    }

    public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
}
