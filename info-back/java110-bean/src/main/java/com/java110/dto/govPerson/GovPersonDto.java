package com.java110.dto.govPerson;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 人口管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovPersonDto extends PageDto implements Serializable {

    private String birthday;
private String idType;
private String nation;
private String idCard;
private String prePersonName;
private String personSex;
private String ramark;
private String personName;
private String govPersonId;
private String caId;
private String nativePlace;
private String politicalOutlook;
private String personType;
private String personTel;
private String religiousBelief;
private String maritalStatus;
private String caName;
private String personTypeName;


    private Date createTime;

    private String statusCd = "0";


    public String getBirthday() {
        return birthday;
    }
public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
public String getIdType() {
        return idType;
    }
public void setIdType(String idType) {
        this.idType = idType;
    }
public String getNation() {
        return nation;
    }
public void setNation(String nation) {
        this.nation = nation;
    }
public String getIdCard() {
        return idCard;
    }
public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
public String getPrePersonName() {
        return prePersonName;
    }
public void setPrePersonName(String prePersonName) {
        this.prePersonName = prePersonName;
    }
public String getPersonSex() {
        return personSex;
    }
public void setPersonSex(String personSex) {
        this.personSex = personSex;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getNativePlace() {
        return nativePlace;
    }
public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }
public String getPoliticalOutlook() {
        return politicalOutlook;
    }
public void setPoliticalOutlook(String politicalOutlook) {
        this.politicalOutlook = politicalOutlook;
    }
public String getPersonType() {
        return personType;
    }
public void setPersonType(String personType) {
        this.personType = personType;
    }
public String getPersonTel() {
        return personTel;
    }
public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }
public String getReligiousBelief() {
        return religiousBelief;
    }
public void setReligiousBelief(String religiousBelief) {
        this.religiousBelief = religiousBelief;
    }
public String getMaritalStatus() {
        return maritalStatus;
    }
public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getCaName() {
        return caName;
    }

    public void setCaName(String caName) {
        this.caName = caName;
    }

    public String getPersonTypeName() {
        return personTypeName;
    }

    public void setPersonTypeName(String personTypeName) {
        this.personTypeName = personTypeName;
    }
    private String datasourceType = "999999";
    public String getDatasourceType() {
        return datasourceType;
    }

    public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
}
