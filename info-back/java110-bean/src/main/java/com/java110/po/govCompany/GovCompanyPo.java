package com.java110.po.govCompany;

import java.io.Serializable;
import java.util.Date;

public class GovCompanyPo implements Serializable {

    private String companyType;
private String registerTime;
private String idCard;
private String companyName;
private String personIdCard;
private String statusCd = "0";
private String ramark;
private String artificialPerson;
private String personName;
private String datasourceType= "999999";
private String govCompanyId;
private String caId;
private String companyAddress;
private String personTel;
public String getCompanyType() {
        return companyType;
    }
public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }
public String getRegisterTime() {
        return registerTime;
    }
public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }
public String getIdCard() {
        return idCard;
    }
public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
public String getCompanyName() {
        return companyName;
    }
public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
public String getPersonIdCard() {
        return personIdCard;
    }
public void setPersonIdCard(String personIdCard) {
        this.personIdCard = personIdCard;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getArtificialPerson() {
        return artificialPerson;
    }
public void setArtificialPerson(String artificialPerson) {
        this.artificialPerson = artificialPerson;
    }
public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getGovCompanyId() {
        return govCompanyId;
    }
public void setGovCompanyId(String govCompanyId) {
        this.govCompanyId = govCompanyId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getCompanyAddress() {
        return companyAddress;
    }
public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }
public String getPersonTel() {
        return personTel;
    }
public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }



}
