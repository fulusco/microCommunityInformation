package com.java110.po.govWorkType;

import java.io.Serializable;
import java.util.Date;

public class GovWorkTypePo implements Serializable {

    private String workTypeName;
private String statusCd = "0";
private String state;
private String govTypeId;
private String ramark;
public String getWorkTypeName() {
        return workTypeName;
    }
public void setWorkTypeName(String workTypeName) {
        this.workTypeName = workTypeName;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getGovTypeId() {
        return govTypeId;
    }
public void setGovTypeId(String govTypeId) {
        this.govTypeId = govTypeId;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }



}
