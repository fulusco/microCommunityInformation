package com.java110.po.govOwnerPerson;

import java.io.Serializable;
import java.util.Date;

public class GovOwnerPersonPo implements Serializable {

    private String govPersonId;
private String caId;
private String govOpId;
private String statusCd = "0";
private String govOwnerId;
private String relCd;
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getGovOpId() {
        return govOpId;
    }
public void setGovOpId(String govOpId) {
        this.govOpId = govOpId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getGovOwnerId() {
        return govOwnerId;
    }
public void setGovOwnerId(String govOwnerId) {
        this.govOwnerId = govOwnerId;
    }
public String getRelCd() {
        return relCd;
    }
public void setRelCd(String relCd) {
        this.relCd = relCd;
    }
    private String datasourceType = "999999";
    public String getDatasourceType() {
        return datasourceType;
    }

    public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }


}
