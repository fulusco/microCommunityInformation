package com.java110.po.govOwner;

import java.io.Serializable;
import java.util.Date;

public class GovOwnerPo implements Serializable {

    private String ownerType;
private String idCard;
private String ownerTel;
private String statusCd = "0";
private String roomId;
private String ramark;
private String ownerNum;
private String roomNum;
private String ownerName;
private String caId;
private String ownerAddress;
private String govOwnerId;
public String getOwnerType() {
        return ownerType;
    }
public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }
public String getIdCard() {
        return idCard;
    }
public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
public String getOwnerTel() {
        return ownerTel;
    }
public void setOwnerTel(String ownerTel) {
        this.ownerTel = ownerTel;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getRoomId() {
        return roomId;
    }
public void setRoomId(String roomId) {
        this.roomId = roomId;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getOwnerNum() {
        return ownerNum;
    }
public void setOwnerNum(String ownerNum) {
        this.ownerNum = ownerNum;
    }
public String getRoomNum() {
        return roomNum;
    }
public void setRoomNum(String roomNum) {
        this.roomNum = roomNum;
    }
public String getOwnerName() {
        return ownerName;
    }
public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getOwnerAddress() {
        return ownerAddress;
    }
public void setOwnerAddress(String ownerAddress) {
        this.ownerAddress = ownerAddress;
    }
public String getGovOwnerId() {
        return govOwnerId;
    }
public void setGovOwnerId(String govOwnerId) {
        this.govOwnerId = govOwnerId;
    }


    private String datasourceType = "999999";
    public String getDatasourceType() {
        return datasourceType;
    }

    public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
}
