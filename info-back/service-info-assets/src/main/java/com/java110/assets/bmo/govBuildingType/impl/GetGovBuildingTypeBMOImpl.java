package com.java110.assets.bmo.govBuildingType.impl;


import com.java110.assets.bmo.govBuildingType.IGetGovBuildingTypeBMO;
import com.java110.intf.assets.IGovBuildingTypeInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govBuildingType.GovBuildingTypeDto;

import java.util.ArrayList;
import java.util.List;

@Service("getGovBuildingTypeBMOImpl")
public class GetGovBuildingTypeBMOImpl implements IGetGovBuildingTypeBMO {

    @Autowired
    private IGovBuildingTypeInnerServiceSMO govBuildingTypeInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govBuildingTypeDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovBuildingTypeDto govBuildingTypeDto) {


        int count = govBuildingTypeInnerServiceSMOImpl.queryGovBuildingTypesCount(govBuildingTypeDto);

        List<GovBuildingTypeDto> govBuildingTypeDtos = null;
        if (count > 0) {
            govBuildingTypeDtos = govBuildingTypeInnerServiceSMOImpl.queryGovBuildingTypes(govBuildingTypeDto);
        } else {
            govBuildingTypeDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govBuildingTypeDto.getRow()), count, govBuildingTypeDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
