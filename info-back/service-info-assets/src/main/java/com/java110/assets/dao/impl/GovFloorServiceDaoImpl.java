package com.java110.assets.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.assets.dao.IGovFloorServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 建筑物管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govFloorServiceDaoImpl")
//@Transactional
public class GovFloorServiceDaoImpl extends BaseServiceDao implements IGovFloorServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovFloorServiceDaoImpl.class);





    /**
     * 保存建筑物管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovFloorInfo(Map info) throws DAOException {
        logger.debug("保存建筑物管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govFloorServiceDaoImpl.saveGovFloorInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存建筑物管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询建筑物管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovFloorInfo(Map info) throws DAOException {
        logger.debug("查询建筑物管理信息 入参 info : {}",info);

        List<Map> businessGovFloorInfos = sqlSessionTemplate.selectList("govFloorServiceDaoImpl.getGovFloorInfo",info);

        return businessGovFloorInfos;
    }


    /**
     * 修改建筑物管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovFloorInfo(Map info) throws DAOException {
        logger.debug("修改建筑物管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govFloorServiceDaoImpl.updateGovFloorInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改建筑物管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询建筑物管理数量
     * @param info 建筑物管理信息
     * @return 建筑物管理数量
     */
    @Override
    public int queryGovFloorsCount(Map info) {
        logger.debug("查询建筑物管理数据 入参 info : {}",info);

        List<Map> businessGovFloorInfos = sqlSessionTemplate.selectList("govFloorServiceDaoImpl.queryGovFloorsCount", info);
        if (businessGovFloorInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovFloorInfos.get(0).get("count").toString());
    }


}
