package com.java110.assets.bmo.govCommunity.impl;


import com.java110.assets.bmo.govCommunity.IGetGovCommunityBMO;
import com.java110.intf.assets.IGovCommunityInnerServiceSMO;
import com.java110.utils.cache.MappingCache;
import com.java110.utils.util.StringUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.java110.dto.govCommunity.GovCommunityDto;


import java.util.ArrayList;
import java.util.List;

@Service("getGovCommunityBMOImpl")
public class GetGovCommunityBMOImpl implements IGetGovCommunityBMO {

    @Autowired
    private IGovCommunityInnerServiceSMO govCommunityInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govCommunityDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovCommunityDto govCommunityDto) {


        int count = govCommunityInnerServiceSMOImpl.queryGovCommunitysCount(govCommunityDto);

        List<GovCommunityDto> govCommunityDtos = null;
        if (count > 0) {
            govCommunityDtos = govCommunityInnerServiceSMOImpl.queryGovCommunitys(govCommunityDto);
            freshShopImg(govCommunityDtos);
        } else {
            govCommunityDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govCommunityDto.getRow()), count, govCommunityDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

    /**
     * @param govCommunityDtos
     */
    private void freshShopImg(List<GovCommunityDto> govCommunityDtos) {
        String imgUrl = MappingCache.getValue("IMG_PATH");

        imgUrl += (!StringUtil.isEmpty(imgUrl) && imgUrl.endsWith("/") ? "" : "/");

        for (GovCommunityDto govCommunityDto : govCommunityDtos) {
            govCommunityDto.setCommunityIcon(imgUrl + govCommunityDto.getCommunityIcon());
        }
    }

}
