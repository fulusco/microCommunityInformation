package com.java110.assets.bmo.govRoom;
import com.java110.dto.govRoom.GovRoomDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovRoomBMO {


    /**
     * 查询房屋管理
     * add by wuxw
     * @param  govRoomDto
     * @return
     */
    ResponseEntity<String> get(GovRoomDto govRoomDto);


}
