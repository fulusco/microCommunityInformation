package com.java110.assets.api;


import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.govBuildingType.IDeleteGovBuildingTypeBMO;
import com.java110.assets.bmo.govBuildingType.IGetGovBuildingTypeBMO;
import com.java110.assets.bmo.govBuildingType.ISaveGovBuildingTypeBMO;
import com.java110.assets.bmo.govBuildingType.IUpdateGovBuildingTypeBMO;
import com.java110.dto.govBuildingType.GovBuildingTypeDto;
import com.java110.po.govBuildingType.GovBuildingTypePo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govBuildingType")
public class GovBuildingTypeApi {

    @Autowired
    private ISaveGovBuildingTypeBMO saveGovBuildingTypeBMOImpl;
    @Autowired
    private IUpdateGovBuildingTypeBMO updateGovBuildingTypeBMOImpl;
    @Autowired
    private IDeleteGovBuildingTypeBMO deleteGovBuildingTypeBMOImpl;

    @Autowired
    private IGetGovBuildingTypeBMO getGovBuildingTypeBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govBuildingType/saveGovBuildingType
     * @path /app/govBuildingType/saveGovBuildingType
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovBuildingType", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovBuildingType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "typeName", "请求报文中未包含typeName");

        GovBuildingTypePo govBuildingTypePo = BeanConvertUtil.covertBean(reqJson, GovBuildingTypePo.class);
        return saveGovBuildingTypeBMOImpl.save(govBuildingTypePo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govBuildingType/updateGovBuildingType
     * @path /app/govBuildingType/updateGovBuildingType
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovBuildingType", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovBuildingType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "typeName", "请求报文中未包含typeName");
        Assert.hasKeyAndValue(reqJson, "typeId", "typeId不能为空");


        GovBuildingTypePo govBuildingTypePo = BeanConvertUtil.covertBean(reqJson, GovBuildingTypePo.class);
        return updateGovBuildingTypeBMOImpl.update(govBuildingTypePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govBuildingType/deleteGovBuildingType
     * @path /app/govBuildingType/deleteGovBuildingType
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovBuildingType", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovBuildingType(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "typeId", "typeId不能为空");


        GovBuildingTypePo govBuildingTypePo = BeanConvertUtil.covertBean(reqJson, GovBuildingTypePo.class);
        return deleteGovBuildingTypeBMOImpl.delete(govBuildingTypePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govBuildingType/queryGovBuildingType
     * @path /app/govBuildingType/queryGovBuildingType
     * @param
     * @return
     */
    @RequestMapping(value = "/queryGovBuildingType", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovBuildingType(@RequestParam(value = "typeName", required = false) String typeName,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovBuildingTypeDto govBuildingTypeDto = new GovBuildingTypeDto();
        govBuildingTypeDto.setPage(page);
        govBuildingTypeDto.setRow(row);
        govBuildingTypeDto.setTypeName( typeName );
        return getGovBuildingTypeBMOImpl.get(govBuildingTypeDto);
    }
}
