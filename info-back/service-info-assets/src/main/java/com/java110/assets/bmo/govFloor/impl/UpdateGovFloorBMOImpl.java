package com.java110.assets.bmo.govFloor.impl;


import com.java110.assets.bmo.govFloor.IUpdateGovFloorBMO;
import com.java110.core.annotation.Java110Transactional;

import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.file.FileDto;
import com.java110.intf.assets.IGovFloorInnerServiceSMO;
import com.java110.intf.engine.IFileInnerServiceSMO;
import com.java110.intf.engine.IFileRelInnerServiceSMO;
import com.java110.po.file.FileRelPo;
import com.java110.po.govFloor.GovFloorPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("updateGovFloorBMOImpl")
public class UpdateGovFloorBMOImpl implements IUpdateGovFloorBMO {

    @Autowired
    private IGovFloorInnerServiceSMO govFloorInnerServiceSMOImpl;
    @Autowired
    private IFileInnerServiceSMO fileInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;
    /**
     *
     *
     * @param govFloorPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovFloorPo govFloorPo) {
        FileDto fileDto = new FileDto();
        fileDto.setContext(govFloorPo.getFloorIcon());
        String fileName = fileInnerServiceSMOImpl.saveFile( fileDto );
        //删除原文件关系
        FileRelPo fileRelPo = new FileRelPo();
        fileRelPo.setObjId( govFloorPo.getGovFloorId()  );
        fileRelPo.setFileRealName( govFloorPo.getOldFloorIcon()  );
        fileRelPo.setFileSaveName( govFloorPo.getOldFloorIcon()  );
        fileRelInnerServiceSMOImpl.deleteFileRel( fileRelPo );

        govFloorPo.setFloorIcon( fileName );
        int flag = govFloorInnerServiceSMOImpl.updateGovFloor(govFloorPo);

        if (flag > 0) {

            FileRelPo fileRelPos = new FileRelPo();
            fileRelPos.setFileRelId( GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_fileRelId) );
            fileRelPos.setFileRealName( fileName );
            fileRelPos.setFileSaveName( fileName );
            fileRelPos.setObjId( govFloorPo.getGovFloorId() );
            fileRelPos.setRelTypeCd( "90200" );
            fileRelPos.setSaveWay( "ftp" );
            fileRelInnerServiceSMOImpl.saveFileRel( fileRelPos );
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
