package com.java110.assets.bmo.govCommunity;
import com.java110.dto.govCommunity.GovCommunityDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovCommunityBMO {


    /**
     * 查询小区信息
     * add by wuxw
     * @param  govCommunityDto
     * @return
     */
    ResponseEntity<String> get(GovCommunityDto govCommunityDto);


}
