package com.java110.assets.bmo.govCommunity.impl;

import com.java110.assets.bmo.govCommunity.ISaveGovCommunityBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;

import com.java110.dto.file.FileDto;
import com.java110.intf.assets.IGovCommunityInnerServiceSMO;
import com.java110.intf.engine.IFileInnerServiceSMO;
import com.java110.intf.engine.IFileRelInnerServiceSMO;
import com.java110.po.file.FileRelPo;
import com.java110.po.govCommunity.GovCommunityPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;



@Service("saveGovCommunityBMOImpl")
public class SaveGovCommunityBMOImpl implements ISaveGovCommunityBMO {

    @Autowired
    private IGovCommunityInnerServiceSMO govCommunityInnerServiceSMOImpl;
    @Autowired
    private IFileInnerServiceSMO fileInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;
    /**
     * 添加小区信息
     *
     * @param govCommunityPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovCommunityPo govCommunityPo) {

        //上传文件
        FileDto fileDto = new FileDto();
        fileDto.setContext(govCommunityPo.getCommunityIcon());
        String fileName = fileInnerServiceSMOImpl.saveFile( fileDto );

        govCommunityPo.setGovCommunityId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govCommunityId));
        govCommunityPo.setCommunityIcon( fileName );
        int flag = govCommunityInnerServiceSMOImpl.saveGovCommunity(govCommunityPo);

        if (flag > 0) {
            //保存文件与业务之间关系
            FileRelPo fileRelPo = new FileRelPo();
            fileRelPo.setFileRelId( GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_fileRelId) );
            fileRelPo.setFileRealName( fileName );
            fileRelPo.setFileSaveName( fileName );
            fileRelPo.setObjId( govCommunityPo.getGovCommunityId() );
            fileRelPo.setRelTypeCd( "90100" );
            fileRelPo.setSaveWay( "ftp" );
            fileRelInnerServiceSMOImpl.saveFileRel( fileRelPo );
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
