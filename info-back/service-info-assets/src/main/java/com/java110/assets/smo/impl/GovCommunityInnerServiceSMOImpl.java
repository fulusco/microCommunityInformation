package com.java110.assets.smo.impl;


import com.java110.assets.dao.IGovCommunityServiceDao;
import com.java110.dto.govCommunity.GovCommunityDto;
import com.java110.intf.assets.IGovCommunityInnerServiceSMO;
import com.java110.po.govCommunity.GovCommunityPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 小区信息内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovCommunityInnerServiceSMOImpl extends BaseServiceSMO implements IGovCommunityInnerServiceSMO {

    @Autowired
    private IGovCommunityServiceDao govCommunityServiceDaoImpl;


    @Override
    public int saveGovCommunity(@RequestBody GovCommunityPo govCommunityPo) {
        int saveFlag = 1;
        govCommunityServiceDaoImpl.saveGovCommunityInfo(BeanConvertUtil.beanCovertMap(govCommunityPo));
        return saveFlag;
    }

     @Override
    public int updateGovCommunity(@RequestBody  GovCommunityPo govCommunityPo) {
        int saveFlag = 1;
         govCommunityServiceDaoImpl.updateGovCommunityInfo(BeanConvertUtil.beanCovertMap(govCommunityPo));
        return saveFlag;
    }

     @Override
    public int deleteGovCommunity(@RequestBody  GovCommunityPo govCommunityPo) {
        int saveFlag = 1;
        govCommunityPo.setStatusCd("1");
        govCommunityServiceDaoImpl.updateGovCommunityInfo(BeanConvertUtil.beanCovertMap(govCommunityPo));
        return saveFlag;
    }

    @Override
    public List<GovCommunityDto> queryGovCommunitys(@RequestBody  GovCommunityDto govCommunityDto) {

        //校验是否传了 分页信息

        int page = govCommunityDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govCommunityDto.setPage((page - 1) * govCommunityDto.getRow());
        }

        List<GovCommunityDto> govCommunitys = BeanConvertUtil.covertBeanList(govCommunityServiceDaoImpl.getGovCommunityInfo(BeanConvertUtil.beanCovertMap(govCommunityDto)), GovCommunityDto.class);

        return govCommunitys;
    }


    @Override
    public int queryGovCommunitysCount(@RequestBody GovCommunityDto govCommunityDto) {
        return govCommunityServiceDaoImpl.queryGovCommunitysCount(BeanConvertUtil.beanCovertMap(govCommunityDto));    }

    public IGovCommunityServiceDao getGovCommunityServiceDaoImpl() {
        return govCommunityServiceDaoImpl;
    }

    public void setGovCommunityServiceDaoImpl(IGovCommunityServiceDao govCommunityServiceDaoImpl) {
        this.govCommunityServiceDaoImpl = govCommunityServiceDaoImpl;
    }
}
