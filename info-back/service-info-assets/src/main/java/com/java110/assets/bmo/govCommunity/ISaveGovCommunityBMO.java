package com.java110.assets.bmo.govCommunity;

import com.java110.po.govCommunity.GovCommunityPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovCommunityBMO {


    /**
     * 添加小区信息
     * add by wuxw
     * @param govCommunityPo
     * @return
     */
    ResponseEntity<String> save(GovCommunityPo govCommunityPo);


}
