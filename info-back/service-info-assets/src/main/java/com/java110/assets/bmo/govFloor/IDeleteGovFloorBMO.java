package com.java110.assets.bmo.govFloor;
import com.java110.po.govFloor.GovFloorPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovFloorBMO {


    /**
     * 修改建筑物管理
     * add by wuxw
     * @param govFloorPo
     * @return
     */
    ResponseEntity<String> delete(GovFloorPo govFloorPo);


}
