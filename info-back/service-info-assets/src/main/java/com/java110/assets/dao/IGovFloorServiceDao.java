package com.java110.assets.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 建筑物管理组件内部之间使用，没有给外围系统提供服务能力
 * 建筑物管理服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovFloorServiceDao {


    /**
     * 保存 建筑物管理信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovFloorInfo(Map info) throws DAOException;




    /**
     * 查询建筑物管理信息（instance过程）
     * 根据bId 查询建筑物管理信息
     * @param info bId 信息
     * @return 建筑物管理信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovFloorInfo(Map info) throws DAOException;



    /**
     * 修改建筑物管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovFloorInfo(Map info) throws DAOException;


    /**
     * 查询建筑物管理总数
     *
     * @param info 建筑物管理信息
     * @return 建筑物管理数量
     */
    int queryGovFloorsCount(Map info);

}
