package com.java110.assets.bmo.govRoom;
import com.java110.po.govRoom.GovRoomPo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovRoomBMO {


    /**
     * 修改房屋管理
     * add by wuxw
     * @param govRoomPo
     * @return
     */
    ResponseEntity<String> update(GovRoomPo govRoomPo);


}
