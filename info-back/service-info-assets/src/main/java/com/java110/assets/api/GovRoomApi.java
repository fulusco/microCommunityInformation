package com.java110.assets.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.govRoom.IDeleteGovRoomBMO;
import com.java110.assets.bmo.govRoom.IGetGovRoomBMO;
import com.java110.assets.bmo.govRoom.ISaveGovRoomBMO;
import com.java110.assets.bmo.govRoom.IUpdateGovRoomBMO;
import com.java110.dto.govRoom.GovRoomDto;
import com.java110.po.govRoom.GovRoomPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govRoom")
public class GovRoomApi {

    @Autowired
    private ISaveGovRoomBMO saveGovRoomBMOImpl;
    @Autowired
    private IUpdateGovRoomBMO updateGovRoomBMOImpl;
    @Autowired
    private IDeleteGovRoomBMO deleteGovRoomBMOImpl;

    @Autowired
    private IGetGovRoomBMO getGovRoomBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govRoom/saveGovRoom
     * @path /app/govRoom/saveGovRoom
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovRoom", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovRoom(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "govFloorId", "请求报文中未包含govFloorId");
        Assert.hasKeyAndValue(reqJson, "roomNum", "请求报文中未包含roomNum");
        Assert.hasKeyAndValue(reqJson, "roomType", "请求报文中未包含roomType");
        Assert.hasKeyAndValue(reqJson, "roomArea", "请求报文中未包含roomArea");
        Assert.hasKeyAndValue(reqJson, "layer", "请求报文中未包含layer");
        Assert.hasKeyAndValue(reqJson, "roomAddress", "请求报文中未包含roomAddress");
        Assert.hasKeyAndValue(reqJson, "roomRight", "请求报文中未包含roomRight");
        Assert.hasKeyAndValue(reqJson, "isConservati", "请求报文中未包含isConservati");
        Assert.hasKeyAndValue(reqJson, "isSettle", "请求报文中未包含isSettle");


        GovRoomPo govRoomPo = BeanConvertUtil.covertBean(reqJson, GovRoomPo.class);
        return saveGovRoomBMOImpl.save(govRoomPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govRoom/updateGovRoom
     * @path /app/govRoom/updateGovRoom
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovRoom", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovRoom(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "govFloorId", "请求报文中未包含govFloorId");
        Assert.hasKeyAndValue(reqJson, "roomNum", "请求报文中未包含roomNum");
        Assert.hasKeyAndValue(reqJson, "roomType", "请求报文中未包含roomType");
        Assert.hasKeyAndValue(reqJson, "roomArea", "请求报文中未包含roomArea");
        Assert.hasKeyAndValue(reqJson, "layer", "请求报文中未包含layer");
        Assert.hasKeyAndValue(reqJson, "roomAddress", "请求报文中未包含roomAddress");
        Assert.hasKeyAndValue(reqJson, "roomRight", "请求报文中未包含roomRight");
        Assert.hasKeyAndValue(reqJson, "isConservati", "请求报文中未包含isConservati");
        Assert.hasKeyAndValue(reqJson, "isSettle", "请求报文中未包含isSettle");
        Assert.hasKeyAndValue(reqJson, "govRoomId", "govRoomId不能为空");


        GovRoomPo govRoomPo = BeanConvertUtil.covertBean(reqJson, GovRoomPo.class);
        return updateGovRoomBMOImpl.update(govRoomPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govRoom/deleteGovRoom
     * @path /app/govRoom/deleteGovRoom
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovRoom", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovRoom(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govRoomId", "govRoomId不能为空");


        GovRoomPo govRoomPo = BeanConvertUtil.covertBean(reqJson, GovRoomPo.class);
        return deleteGovRoomBMOImpl.delete(govRoomPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govRoom/queryGovRoom
     * @path /app/govRoom/queryGovRoom
     * @param
     * @return
     */
    @RequestMapping(value = "/queryGovRoom", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovRoom(@RequestParam(value = "govFloorId" , required = false) String govFloorId,
                                                      @RequestParam(value = "caId" , required = false) String caId,
                                                      @RequestParam(value = "govCommunityId" , required = false) String govCommunityId,
                                                      @RequestParam(value = "roomNum" , required = false) String roomNum,
                                                      @RequestParam(value = "roomType" , required = false) String roomType,
                                                      @RequestParam(value = "roomRight" , required = false) String roomRight,
                                                      @RequestParam(value = "ownerName" , required = false) String ownerName,
                                                      @RequestParam(value = "ownerTel" , required = false) String ownerTel,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovRoomDto govRoomDto = new GovRoomDto();
        govRoomDto.setPage(page);
        govRoomDto.setRow(row);
        govRoomDto.setGovFloorId(govFloorId);
        govRoomDto.setCaId(caId);
        govRoomDto.setGovCommunityId(govCommunityId);
        govRoomDto.setRoomNum(roomNum);
        govRoomDto.setRoomType(roomType);
        govRoomDto.setRoomRight(roomRight);
        govRoomDto.setOwnerName(ownerName);
        govRoomDto.setOwnerTel(ownerTel);
        return getGovRoomBMOImpl.get(govRoomDto);
    }
}
