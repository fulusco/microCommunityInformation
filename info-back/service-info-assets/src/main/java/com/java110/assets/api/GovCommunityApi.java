package com.java110.assets.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.govCommunity.IDeleteGovCommunityBMO;
import com.java110.assets.bmo.govCommunity.IGetGovCommunityBMO;
import com.java110.assets.bmo.govCommunity.ISaveGovCommunityBMO;
import com.java110.assets.bmo.govCommunity.IUpdateGovCommunityBMO;
import com.java110.dto.govCommunity.GovCommunityDto;
import com.java110.po.govCommunity.GovCommunityPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govCommunity")
public class GovCommunityApi {

    @Autowired
    private ISaveGovCommunityBMO saveGovCommunityBMOImpl;
    @Autowired
    private IUpdateGovCommunityBMO updateGovCommunityBMOImpl;
    @Autowired
    private IDeleteGovCommunityBMO deleteGovCommunityBMOImpl;

    @Autowired
    private IGetGovCommunityBMO getGovCommunityBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govCommunity/saveGovCommunity
     * @path /app/govCommunity/saveGovCommunity
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovCommunity", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovCommunity(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "communityName", "请求报文中未包含communityName");
        Assert.hasKeyAndValue(reqJson, "propertyType", "请求报文中未包含propertyType");
        Assert.hasKeyAndValue(reqJson, "managerName", "请求报文中未包含managerName");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personLink", "请求报文中未包含personLink");
        Assert.hasKeyAndValue(reqJson, "communityIcon", "请求报文中未包含communityIcon");
        Assert.hasKeyAndValue(reqJson, "communitySecure", "请求报文中未包含communitySecure");


        GovCommunityPo govCommunityPo = BeanConvertUtil.covertBean(reqJson, GovCommunityPo.class);
        return saveGovCommunityBMOImpl.save(govCommunityPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govCommunity/updateGovCommunity
     * @path /app/govCommunity/updateGovCommunity
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovCommunity", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovCommunity(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "communityName", "请求报文中未包含communityName");
        Assert.hasKeyAndValue(reqJson, "propertyType", "请求报文中未包含propertyType");
        Assert.hasKeyAndValue(reqJson, "managerName", "请求报文中未包含managerName");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personLink", "请求报文中未包含personLink");
        Assert.hasKeyAndValue(reqJson, "communityIcon", "请求报文中未包含communityIcon");
        Assert.hasKeyAndValue(reqJson, "communitySecure", "请求报文中未包含communitySecure");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "govCommunityId不能为空");


        GovCommunityPo govCommunityPo = BeanConvertUtil.covertBean(reqJson, GovCommunityPo.class);
        return updateGovCommunityBMOImpl.update(govCommunityPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govCommunity/deleteGovCommunity
     * @path /app/govCommunity/deleteGovCommunity
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovCommunity", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovCommunity(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govCommunityId", "govCommunityId不能为空");


        GovCommunityPo govCommunityPo = BeanConvertUtil.covertBean(reqJson, GovCommunityPo.class);
        return deleteGovCommunityBMOImpl.delete(govCommunityPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govCommunity/queryGovCommunity
     * @path /app/govCommunity/queryGovCommunity
     * @param
     * @return
     */
    @RequestMapping(value = "/queryGovCommunity", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovCommunity(@RequestParam(value = "communityName", required = false) String communityName,
                                                      @RequestParam(value = "propertyType", required = false) String propertyType,
                                                      @RequestParam(value = "managerName", required = false) String managerName,
                                                      @RequestParam(value = "caId", required = false) String caId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovCommunityDto govCommunityDto = new GovCommunityDto();
        govCommunityDto.setPage(page);
        govCommunityDto.setRow(row);
        govCommunityDto.setCommunityName(communityName);
        govCommunityDto.setPropertyType(propertyType);
        govCommunityDto.setManagerName(managerName);
        govCommunityDto.setCaId(caId);
        return getGovCommunityBMOImpl.get(govCommunityDto);
    }
}
