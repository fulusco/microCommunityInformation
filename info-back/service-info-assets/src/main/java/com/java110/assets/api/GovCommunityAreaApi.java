package com.java110.assets.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.govCommunityArea.IDeleteGovCommunityAreaBMO;
import com.java110.assets.bmo.govCommunityArea.IGetGovCommunityAreaBMO;
import com.java110.assets.bmo.govCommunityArea.ISaveGovCommunityAreaBMO;
import com.java110.assets.bmo.govCommunityArea.IUpdateGovCommunityAreaBMO;
import com.java110.dto.govCommunityArea.GovCommunityAreaDto;
import com.java110.po.govCommunityArea.GovCommunityAreaPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/govCommunityArea")
public class GovCommunityAreaApi {

    @Autowired
    private ISaveGovCommunityAreaBMO saveGovCommunityAreaBMOImpl;
    @Autowired
    private IUpdateGovCommunityAreaBMO updateGovCommunityAreaBMOImpl;
    @Autowired
    private IDeleteGovCommunityAreaBMO deleteGovCommunityAreaBMOImpl;

    @Autowired
    private IGetGovCommunityAreaBMO getGovCommunityAreaBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govCommunityArea/saveGovCommunityArea
     * @path /app/govCommunityArea/saveGovCommunityArea
     */
    @RequestMapping(value = "/saveGovCommunityArea", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovCommunityArea( @RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caCode", "请求报文中未包含caCode");
        Assert.hasKeyAndValue(reqJson, "caName", "请求报文中未包含caName");
        Assert.hasKeyAndValue(reqJson, "caSpace", "请求报文中未包含caSpace");
        Assert.hasKeyAndValue(reqJson, "areaCode", "请求报文中未包含areaCode");
        Assert.hasKeyAndValue(reqJson, "caAddress", "请求报文中未包含caAddress");
        Assert.hasKeyAndValue(reqJson, "person", "请求报文中未包含person");
        Assert.hasKeyAndValue(reqJson, "personLink", "请求报文中未包含personLink");


        GovCommunityAreaPo govCommunityAreaPo = BeanConvertUtil.covertBean(reqJson, GovCommunityAreaPo.class);
        return saveGovCommunityAreaBMOImpl.save(govCommunityAreaPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govCommunityArea/updateGovCommunityArea
     * @path /app/govCommunityArea/updateGovCommunityArea
     */
    @RequestMapping(value = "/updateGovCommunityArea", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovCommunityArea(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caCode", "请求报文中未包含caCode");
        Assert.hasKeyAndValue(reqJson, "caName", "请求报文中未包含caName");
        Assert.hasKeyAndValue(reqJson, "caSpace", "请求报文中未包含caSpace");
        Assert.hasKeyAndValue(reqJson, "areaCode", "请求报文中未包含areaCode");
        Assert.hasKeyAndValue(reqJson, "caAddress", "请求报文中未包含caAddress");
        Assert.hasKeyAndValue(reqJson, "person", "请求报文中未包含person");
        Assert.hasKeyAndValue(reqJson, "personLink", "请求报文中未包含personLink");
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");


        GovCommunityAreaPo govCommunityAreaPo = BeanConvertUtil.covertBean(reqJson, GovCommunityAreaPo.class);
        return updateGovCommunityAreaBMOImpl.update(govCommunityAreaPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govCommunityArea/deleteGovCommunityArea
     * @path /app/govCommunityArea/deleteGovCommunityArea
     */
    @RequestMapping(value = "/deleteGovCommunityArea", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovCommunityArea(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");


        GovCommunityAreaPo govCommunityAreaPo = BeanConvertUtil.covertBean(reqJson, GovCommunityAreaPo.class);
        return deleteGovCommunityAreaBMOImpl.delete(govCommunityAreaPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /govCommunityArea/queryGovCommunityArea
     * @path /app/govCommunityArea/queryGovCommunityArea
     */
    @RequestMapping(value = "/queryGovCommunityArea", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovCommunityArea(@RequestParam(value = "caName", required = false) String caName,
                                                        @RequestParam(value = "areaCode", required = false) String areaCode,
                                                        @RequestParam(value = "person", required = false) String person,
                                                        @RequestParam(value = "personLink", required = false) String personLink,
                                                        @RequestParam(value = "page") int page,
                                                        @RequestParam(value = "row") int row) {
        GovCommunityAreaDto govCommunityAreaDto = new GovCommunityAreaDto();
        govCommunityAreaDto.setPage(page);
        govCommunityAreaDto.setRow(row);
        govCommunityAreaDto.setCaCode(areaCode);
        govCommunityAreaDto.setCaName(caName);
        govCommunityAreaDto.setPerson(person);
        govCommunityAreaDto.setPersonLink(personLink);
        return getGovCommunityAreaBMOImpl.get(govCommunityAreaDto);
    }
}
