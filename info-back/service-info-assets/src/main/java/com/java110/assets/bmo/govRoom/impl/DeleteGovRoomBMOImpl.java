package com.java110.assets.bmo.govRoom.impl;

import com.java110.assets.bmo.govRoom.IDeleteGovRoomBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.intf.assets.IGovRoomInnerServiceSMO;
import com.java110.po.govRoom.GovRoomPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovRoomBMOImpl")
public class DeleteGovRoomBMOImpl implements IDeleteGovRoomBMO {

    @Autowired
    private IGovRoomInnerServiceSMO govRoomInnerServiceSMOImpl;

    /**
     * @param govRoomPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovRoomPo govRoomPo) {

        int flag = govRoomInnerServiceSMOImpl.deleteGovRoom(govRoomPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
