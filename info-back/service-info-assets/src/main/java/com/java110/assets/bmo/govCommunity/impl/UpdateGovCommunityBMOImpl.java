package com.java110.assets.bmo.govCommunity.impl;

import com.java110.assets.bmo.govCommunity.IUpdateGovCommunityBMO;
import com.java110.core.annotation.Java110Transactional;

import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.file.FileDto;
import com.java110.intf.assets.IGovCommunityInnerServiceSMO;
import com.java110.intf.engine.IFileInnerServiceSMO;
import com.java110.intf.engine.IFileRelInnerServiceSMO;
import com.java110.po.file.FileRelPo;
import com.java110.po.govCommunity.GovCommunityPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovCommunityBMOImpl")
public class UpdateGovCommunityBMOImpl implements IUpdateGovCommunityBMO {

    @Autowired
    private IGovCommunityInnerServiceSMO govCommunityInnerServiceSMOImpl;
    @Autowired
    private IFileInnerServiceSMO fileInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;
    /**
     *
     *
     * @param govCommunityPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovCommunityPo govCommunityPo) {


        FileDto fileDto = new FileDto();
        fileDto.setContext(govCommunityPo.getCommunityIcon());
        String fileName = fileInnerServiceSMOImpl.saveFile( fileDto );
        //删除原文件关系
        FileRelPo fileRelPo = new FileRelPo();
        fileRelPo.setObjId( govCommunityPo.getGovCommunityId()  );
        fileRelPo.setFileRealName( govCommunityPo.getOldCommunityIcon()  );
        fileRelPo.setFileSaveName( govCommunityPo.getOldCommunityIcon()  );
        fileRelInnerServiceSMOImpl.deleteFileRel( fileRelPo );

        govCommunityPo.setCommunityIcon( fileName );
        int flag = govCommunityInnerServiceSMOImpl.updateGovCommunity(govCommunityPo);

        if (flag > 0) {
            FileRelPo fileRelPos = new FileRelPo();
            fileRelPos.setFileRelId( GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_fileRelId) );
            fileRelPos.setFileRealName( fileName );
            fileRelPos.setFileSaveName( fileName );
            fileRelPos.setObjId( govCommunityPo.getGovCommunityId() );
            fileRelPos.setRelTypeCd( "90100" );
            fileRelPos.setSaveWay( "ftp" );
            fileRelInnerServiceSMOImpl.saveFileRel( fileRelPos );
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
