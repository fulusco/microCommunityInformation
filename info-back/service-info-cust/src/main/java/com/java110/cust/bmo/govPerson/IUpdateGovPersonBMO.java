package com.java110.cust.bmo.govPerson;
import com.java110.po.govPerson.GovPersonPo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovPersonBMO {


    /**
     * 修改人口管理
     * add by wuxw
     * @param govPersonPo
     * @return
     */
    ResponseEntity<String> update(GovPersonPo govPersonPo);


}
