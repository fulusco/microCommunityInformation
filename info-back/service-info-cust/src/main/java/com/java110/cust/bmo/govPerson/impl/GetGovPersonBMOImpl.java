package com.java110.cust.bmo.govPerson.impl;

import com.java110.cust.bmo.govPerson.IGetGovPersonBMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govPerson.GovPersonDto;


import java.util.ArrayList;
import java.util.List;

@Service("getGovPersonBMOImpl")
public class GetGovPersonBMOImpl implements IGetGovPersonBMO {

    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govPersonDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovPersonDto govPersonDto) {


        int count = govPersonInnerServiceSMOImpl.queryGovPersonsCount(govPersonDto);

        List<GovPersonDto> govPersonDtos = null;
        if (count > 0) {
            govPersonDtos = govPersonInnerServiceSMOImpl.queryGovPersons(govPersonDto);
        } else {
            govPersonDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPersonDto.getRow()), count, govPersonDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
