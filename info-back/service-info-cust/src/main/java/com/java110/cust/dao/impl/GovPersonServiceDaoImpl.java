package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovPersonServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 人口管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govPersonServiceDaoImpl")
//@Transactional
public class GovPersonServiceDaoImpl extends BaseServiceDao implements IGovPersonServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovPersonServiceDaoImpl.class);





    /**
     * 保存人口管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovPersonInfo(Map info) throws DAOException {
        logger.debug("保存人口管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govPersonServiceDaoImpl.saveGovPersonInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存人口管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询人口管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovPersonInfo(Map info) throws DAOException {
        logger.debug("查询人口管理信息 入参 info : {}",info);

        List<Map> businessGovPersonInfos = sqlSessionTemplate.selectList("govPersonServiceDaoImpl.getGovPersonInfo",info);

        return businessGovPersonInfos;
    }


    /**
     * 修改人口管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovPersonInfo(Map info) throws DAOException {
        logger.debug("修改人口管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govPersonServiceDaoImpl.updateGovPersonInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改人口管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询人口管理数量
     * @param info 人口管理信息
     * @return 人口管理数量
     */
    @Override
    public int queryGovPersonsCount(Map info) {
        logger.debug("查询人口管理数据 入参 info : {}",info);

        List<Map> businessGovPersonInfos = sqlSessionTemplate.selectList("govPersonServiceDaoImpl.queryGovPersonsCount", info);
        if (businessGovPersonInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovPersonInfos.get(0).get("count").toString());
    }


}
