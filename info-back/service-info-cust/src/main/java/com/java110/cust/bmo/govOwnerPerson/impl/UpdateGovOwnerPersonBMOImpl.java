package com.java110.cust.bmo.govOwnerPerson.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.cust.bmo.govOwnerPerson.IUpdateGovOwnerPersonBMO;
import com.java110.intf.cust.IGovOwnerPersonInnerServiceSMO;
import com.java110.po.govOwnerPerson.GovOwnerPersonPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovOwnerPersonBMOImpl")
public class UpdateGovOwnerPersonBMOImpl implements IUpdateGovOwnerPersonBMO {

    @Autowired
    private IGovOwnerPersonInnerServiceSMO govOwnerPersonInnerServiceSMOImpl;

    /**
     *
     *
     * @param govOwnerPersonPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovOwnerPersonPo govOwnerPersonPo) {

        int flag = govOwnerPersonInnerServiceSMOImpl.updateGovOwnerPerson(govOwnerPersonPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
