package com.java110.cust.bmo.govPerson.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.govPerson.ISaveGovPersonBMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveGovPersonBMOImpl")
public class SaveGovPersonBMOImpl implements ISaveGovPersonBMO {

    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govPersonPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovPersonPo govPersonPo) {

        govPersonPo.setGovPersonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govPersonId));
        int flag = govPersonInnerServiceSMOImpl.saveGovPerson(govPersonPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
