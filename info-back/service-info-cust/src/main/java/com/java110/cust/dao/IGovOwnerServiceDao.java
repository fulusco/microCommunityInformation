package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 户籍管理组件内部之间使用，没有给外围系统提供服务能力
 * 户籍管理服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovOwnerServiceDao {


    /**
     * 保存 户籍管理信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovOwnerInfo(Map info) throws DAOException;




    /**
     * 查询户籍管理信息（instance过程）
     * 根据bId 查询户籍管理信息
     * @param info bId 信息
     * @return 户籍管理信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovOwnerInfo(Map info) throws DAOException;



    /**
     * 修改户籍管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovOwnerInfo(Map info) throws DAOException;


    /**
     * 查询户籍管理总数
     *
     * @param info 户籍管理信息
     * @return 户籍管理数量
     */
    int queryGovOwnersCount(Map info);

}
