package com.java110.cust.bmo.govPerson.impl;


import com.java110.core.annotation.Java110Transactional;

import com.java110.cust.bmo.govPerson.IDeleteGovPersonBMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("deleteGovPersonBMOImpl")
public class DeleteGovPersonBMOImpl implements IDeleteGovPersonBMO {

    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;

    /**
     * @param govPersonPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovPersonPo govPersonPo) {

        int flag = govPersonInnerServiceSMOImpl.deleteGovPerson(govPersonPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
