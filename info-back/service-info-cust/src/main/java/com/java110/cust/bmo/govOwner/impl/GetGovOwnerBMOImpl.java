package com.java110.cust.bmo.govOwner.impl;

import com.java110.cust.bmo.govOwner.IGetGovOwnerBMO;
import com.java110.intf.cust.IGovOwnerInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govOwner.GovOwnerDto;

import java.util.ArrayList;
import java.util.List;

@Service("getGovOwnerBMOImpl")
public class GetGovOwnerBMOImpl implements IGetGovOwnerBMO {

    @Autowired
    private IGovOwnerInnerServiceSMO govOwnerInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govOwnerDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovOwnerDto govOwnerDto) {


        int count = govOwnerInnerServiceSMOImpl.queryGovOwnersCount(govOwnerDto);

        List<GovOwnerDto> govOwnerDtos = null;
        if (count > 0) {
            govOwnerDtos = govOwnerInnerServiceSMOImpl.queryGovOwners(govOwnerDto);
        } else {
            govOwnerDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govOwnerDto.getRow()), count, govOwnerDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
