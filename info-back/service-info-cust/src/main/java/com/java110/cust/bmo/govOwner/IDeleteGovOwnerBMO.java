package com.java110.cust.bmo.govOwner;
import com.java110.po.govOwner.GovOwnerPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovOwnerBMO {


    /**
     * 修改户籍管理
     * add by wuxw
     * @param govOwnerPo
     * @return
     */
    ResponseEntity<String> delete(GovOwnerPo govOwnerPo);


}
