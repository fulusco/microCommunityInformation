package com.java110.cust.bmo.govOwner.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.cust.bmo.govOwner.IDeleteGovOwnerBMO;
import com.java110.dto.govOwnerPerson.GovOwnerPersonDto;
import com.java110.intf.cust.IGovOwnerInnerServiceSMO;
import com.java110.intf.cust.IGovOwnerPersonInnerServiceSMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.govOwner.GovOwnerPo;
import com.java110.po.govOwnerPerson.GovOwnerPersonPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deleteGovOwnerBMOImpl")
public class DeleteGovOwnerBMOImpl implements IDeleteGovOwnerBMO {

    @Autowired
    private IGovOwnerInnerServiceSMO govOwnerInnerServiceSMOImpl;
    @Autowired
    private IGovOwnerPersonInnerServiceSMO govOwnerPersonInnerServiceSMOImpl;
    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;
    /**
     * @param govOwnerPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovOwnerPo govOwnerPo) {

        int flag = govOwnerInnerServiceSMOImpl.deleteGovOwner(govOwnerPo);

        if (flag > 0) {
            GovOwnerPersonDto govOwnerPersonDto = new GovOwnerPersonDto();
            govOwnerPersonDto.setGovOwnerId( govOwnerPo.getGovOwnerId() );

            List<GovOwnerPersonDto> govOwnerPersonDtos = govOwnerPersonInnerServiceSMOImpl.queryGovOwnerPersons( govOwnerPersonDto );
            if(null != govOwnerPersonDtos && govOwnerPersonDtos.size() > 0){
                GovOwnerPersonDto govOwnerPerson = govOwnerPersonDtos.get( 0 );
                //删除户籍人口信息到人口表
                GovPersonPo govPersonPo = new GovPersonPo();
                govPersonPo.setGovPersonId( govOwnerPerson.getGovPersonId() );
                govPersonInnerServiceSMOImpl.deleteGovPerson( govPersonPo );
                //删除人口户籍关系表
                GovOwnerPersonPo govOwnerPersonPo = new GovOwnerPersonPo();
                govOwnerPersonPo.setGovOpId( govOwnerPerson.getGovOpId() );
                govOwnerPersonInnerServiceSMOImpl.deleteGovOwnerPerson( govOwnerPersonPo );
            }
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
