package com.java110.cust.bmo.govOwner.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;

import com.java110.cust.bmo.govOwner.ISaveGovOwnerBMO;
import com.java110.intf.cust.IGovOwnerInnerServiceSMO;
import com.java110.intf.cust.IGovOwnerPersonInnerServiceSMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.govOwner.GovOwnerPo;
import com.java110.po.govOwnerPerson.GovOwnerPersonPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.utils.util.DateUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveGovOwnerBMOImpl")
public class SaveGovOwnerBMOImpl implements ISaveGovOwnerBMO {

    @Autowired
    private IGovOwnerInnerServiceSMO govOwnerInnerServiceSMOImpl;
    @Autowired
    private IGovOwnerPersonInnerServiceSMO govOwnerPersonInnerServiceSMOImpl;
    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govOwnerPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovOwnerPo govOwnerPo) {

        govOwnerPo.setGovOwnerId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govOwnerId));
        int flag = govOwnerInnerServiceSMOImpl.saveGovOwner(govOwnerPo);

        if (flag > 0) {
            //保存户籍人口信息到人口表
            GovPersonPo govPersonPo = new GovPersonPo();
            govPersonPo.setGovPersonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govPersonId));
            govPersonPo.setCaId( govOwnerPo.getCaId() );
            govPersonPo.setPersonType( "1001" );
            govPersonPo.setIdType( "1" );
            govPersonPo.setIdCard( govOwnerPo.getIdCard() );
            govPersonPo.setPersonName( govOwnerPo.getOwnerName() );
            govPersonPo.setPersonTel( govOwnerPo.getOwnerTel() );
            govPersonPo.setPersonSex( "1" );
            govPersonPo.setPrePersonName( govOwnerPo.getOwnerName() );
            govPersonPo.setBirthday( DateUtil.getNow( "yyyy-mm-dd hh:mm:ss" ) );
            govPersonPo.setNation( "汉" );
            govPersonPo.setNativePlace( "中国" );
            govPersonPo.setPoliticalOutlook( "无" );
            govPersonPo.setMaritalStatus( "Y" );
            govPersonPo.setReligiousBelief( "无" );
            govPersonPo.setRamark( "添加户籍人口时，系统自动写入户籍人口信息，请完善相关信息" );
            govPersonInnerServiceSMOImpl.saveGovPerson( govPersonPo );

            //保存户籍信息和人口信息关系表
            GovOwnerPersonPo govOwnerPersonPo = new GovOwnerPersonPo();
            govOwnerPersonPo.setGovOpId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govOpId));
            govOwnerPersonPo.setGovOwnerId( govOwnerPo.getGovOwnerId() );
            govOwnerPersonPo.setCaId( govOwnerPo.getCaId() );
            govOwnerPersonPo.setGovPersonId( govPersonPo.getGovPersonId() );
            govOwnerPersonPo.setRelCd( "1001" );
            govOwnerPersonInnerServiceSMOImpl.saveGovOwnerPerson( govOwnerPersonPo );
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
