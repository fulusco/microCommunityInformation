package com.java110.cust.bmo.govPerson;
import com.java110.dto.govPerson.GovPersonDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovPersonBMO {


    /**
     * 查询人口管理
     * add by wuxw
     * @param  govPersonDto
     * @return
     */
    ResponseEntity<String> get(GovPersonDto govPersonDto);


}
