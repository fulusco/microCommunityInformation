package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govCompany.IDeleteGovCompanyBMO;
import com.java110.cust.bmo.govCompany.IGetGovCompanyBMO;
import com.java110.cust.bmo.govCompany.ISaveGovCompanyBMO;
import com.java110.cust.bmo.govCompany.IUpdateGovCompanyBMO;
import com.java110.dto.govCompany.GovCompanyDto;
import com.java110.po.govCompany.GovCompanyPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govCompany")
public class GovCompanyApi {

    @Autowired
    private ISaveGovCompanyBMO saveGovCompanyBMOImpl;
    @Autowired
    private IUpdateGovCompanyBMO updateGovCompanyBMOImpl;
    @Autowired
    private IDeleteGovCompanyBMO deleteGovCompanyBMOImpl;

    @Autowired
    private IGetGovCompanyBMO getGovCompanyBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govCompany/saveGovCompany
     * @path /app/govCompany/saveGovCompany
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovCompany", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovCompany(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "companyName", "请求报文中未包含companyName");
        Assert.hasKeyAndValue(reqJson, "companyType", "请求报文中未包含companyType");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "artificialPerson", "请求报文中未包含artificialPerson");
        Assert.hasKeyAndValue(reqJson, "companyAddress", "请求报文中未包含companyAddress");
        Assert.hasKeyAndValue(reqJson, "registerTime", "请求报文中未包含registerTime");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personIdCard", "请求报文中未包含personIdCard");
        Assert.hasKeyAndValue(reqJson, "personTel", "请求报文中未包含personTel");


        GovCompanyPo govCompanyPo = BeanConvertUtil.covertBean(reqJson, GovCompanyPo.class);
        return saveGovCompanyBMOImpl.save(govCompanyPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govCompany/updateGovCompany
     * @path /app/govCompany/updateGovCompany
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovCompany", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovCompany(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "companyName", "请求报文中未包含companyName");
        Assert.hasKeyAndValue(reqJson, "companyType", "请求报文中未包含companyType");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "artificialPerson", "请求报文中未包含artificialPerson");
        Assert.hasKeyAndValue(reqJson, "companyAddress", "请求报文中未包含companyAddress");
        Assert.hasKeyAndValue(reqJson, "registerTime", "请求报文中未包含registerTime");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personIdCard", "请求报文中未包含personIdCard");
        Assert.hasKeyAndValue(reqJson, "personTel", "请求报文中未包含personTel");
        Assert.hasKeyAndValue(reqJson, "govCompanyId", "govCompanyId不能为空");


        GovCompanyPo govCompanyPo = BeanConvertUtil.covertBean(reqJson, GovCompanyPo.class);
        return updateGovCompanyBMOImpl.update(govCompanyPo);
    }

    /**
     * 微信删除消息模板
         * @serviceCode /govCompany/deleteGovCompany
     * @path /app/govCompany/deleteGovCompany
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovCompany", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovCompany(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govCompanyId", "govCompanyId不能为空");


        GovCompanyPo govCompanyPo = BeanConvertUtil.covertBean(reqJson, GovCompanyPo.class);
        return deleteGovCompanyBMOImpl.delete(govCompanyPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govCompany/queryGovCompany
     * @path /app/govCompany/queryGovCompany
     * @param
     * @return
     */
    @RequestMapping(value = "/queryGovCompany", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovCompany(@RequestParam(value = "caId" , required = false) String caId,
                                                      @RequestParam(value = "companyName" , required = false) String companyName,
                                                      @RequestParam(value = "companyType" , required = false) String companyType,
                                                      @RequestParam(value = "idCard" , required = false) String idCard,
                                                      @RequestParam(value = "artificialPerson" , required = false) String artificialPerson,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovCompanyDto govCompanyDto = new GovCompanyDto();
        govCompanyDto.setPage(page);
        govCompanyDto.setRow(row);
        govCompanyDto.setCaId( caId );
        govCompanyDto.setArtificialPerson( artificialPerson );
        govCompanyDto.setCompanyName( companyName );
        govCompanyDto.setCompanyType( companyType );
        govCompanyDto.setIdCard( idCard );
        return getGovCompanyBMOImpl.get(govCompanyDto);
    }
}
