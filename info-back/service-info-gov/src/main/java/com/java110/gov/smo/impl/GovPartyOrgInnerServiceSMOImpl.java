package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovPartyOrgServiceDao;
import com.java110.dto.govPartyOrg.GovPartyOrgDto;
import com.java110.intf.gov.IGovPartyOrgInnerServiceSMO;
import com.java110.po.govPartyOrg.GovPartyOrgPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 党组织内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovPartyOrgInnerServiceSMOImpl extends BaseServiceSMO implements IGovPartyOrgInnerServiceSMO {

    @Autowired
    private IGovPartyOrgServiceDao govPartyOrgServiceDaoImpl;


    @Override
    public int saveGovPartyOrg(@RequestBody GovPartyOrgPo govPartyOrgPo) {
        int saveFlag = 1;
        govPartyOrgServiceDaoImpl.saveGovPartyOrgInfo(BeanConvertUtil.beanCovertMap(govPartyOrgPo));
        return saveFlag;
    }

     @Override
    public int updateGovPartyOrg(@RequestBody  GovPartyOrgPo govPartyOrgPo) {
        int saveFlag = 1;
         govPartyOrgServiceDaoImpl.updateGovPartyOrgInfo(BeanConvertUtil.beanCovertMap(govPartyOrgPo));
        return saveFlag;
    }

     @Override
    public int deleteGovPartyOrg(@RequestBody  GovPartyOrgPo govPartyOrgPo) {
        int saveFlag = 1;
        govPartyOrgPo.setStatusCd("1");
        govPartyOrgServiceDaoImpl.updateGovPartyOrgInfo(BeanConvertUtil.beanCovertMap(govPartyOrgPo));
        return saveFlag;
    }

    @Override
    public List<GovPartyOrgDto> queryGovPartyOrgs(@RequestBody  GovPartyOrgDto govPartyOrgDto) {

        //校验是否传了 分页信息

        int page = govPartyOrgDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govPartyOrgDto.setPage((page - 1) * govPartyOrgDto.getRow());
        }

        List<GovPartyOrgDto> govPartyOrgs = BeanConvertUtil.covertBeanList(govPartyOrgServiceDaoImpl.getGovPartyOrgInfo(BeanConvertUtil.beanCovertMap(govPartyOrgDto)), GovPartyOrgDto.class);

        return govPartyOrgs;
    }

    @Override
    public List<GovPartyOrgDto> queryNotGovPartyOrg(@RequestBody  GovPartyOrgDto govPartyOrgDto) {

        //校验是否传了 分页信息

        int page = govPartyOrgDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govPartyOrgDto.setPage((page - 1) * govPartyOrgDto.getRow());
        }

        List<GovPartyOrgDto> govPartyOrgs = BeanConvertUtil.covertBeanList(govPartyOrgServiceDaoImpl.getNotGovPartyOrgInfo(BeanConvertUtil.beanCovertMap(govPartyOrgDto)), GovPartyOrgDto.class);

        return govPartyOrgs;
    }


    @Override
    public int queryGovPartyOrgsCount(@RequestBody GovPartyOrgDto govPartyOrgDto) {
        return govPartyOrgServiceDaoImpl.queryGovPartyOrgsCount(BeanConvertUtil.beanCovertMap(govPartyOrgDto));    }

    public IGovPartyOrgServiceDao getGovPartyOrgServiceDaoImpl() {
        return govPartyOrgServiceDaoImpl;
    }

    public void setGovPartyOrgServiceDaoImpl(IGovPartyOrgServiceDao govPartyOrgServiceDaoImpl) {
        this.govPartyOrgServiceDaoImpl = govPartyOrgServiceDaoImpl;
    }
}
