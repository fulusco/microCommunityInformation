package com.java110.gov.bmo.govWorkType;
import com.java110.dto.govWorkType.GovWorkTypeDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovWorkTypeBMO {


    /**
     * 查询党内职务
     * add by wuxw
     * @param  govWorkTypeDto
     * @return
     */
    ResponseEntity<String> get(GovWorkTypeDto govWorkTypeDto);


}
