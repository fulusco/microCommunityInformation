package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govPartyOrg.GovPartyOrgDto;
import com.java110.gov.bmo.govPartyOrg.IDeleteGovPartyOrgBMO;
import com.java110.gov.bmo.govPartyOrg.IGetGovPartyOrgBMO;
import com.java110.gov.bmo.govPartyOrg.ISaveGovPartyOrgBMO;
import com.java110.gov.bmo.govPartyOrg.IUpdateGovPartyOrgBMO;
import com.java110.po.govPartyOrg.GovPartyOrgPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govPartyOrg")
public class GovPartyOrgApi {

    @Autowired
    private ISaveGovPartyOrgBMO saveGovPartyOrgBMOImpl;
    @Autowired
    private IUpdateGovPartyOrgBMO updateGovPartyOrgBMOImpl;
    @Autowired
    private IDeleteGovPartyOrgBMO deleteGovPartyOrgBMOImpl;

    @Autowired
    private IGetGovPartyOrgBMO getGovPartyOrgBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govPartyOrg/saveGovPartyOrg
     * @path /app/govPartyOrg/saveGovPartyOrg
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovPartyOrg", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovPartyOrg(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "orgName", "请求报文中未包含orgName");
        Assert.hasKeyAndValue(reqJson, "preOrgId", "请求报文中未包含preOrgId");
        Assert.hasKeyAndValue(reqJson, "preOrgName", "请求报文中未包含preOrgName");
        Assert.hasKeyAndValue(reqJson, "orgCode", "请求报文中未包含orgCode");
        Assert.hasKeyAndValue(reqJson, "orgSimpleName", "请求报文中未包含orgSimpleName");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personTel", "请求报文中未包含personTel");
        Assert.hasKeyAndValue(reqJson, "address", "请求报文中未包含address");
        Assert.hasKeyAndValue(reqJson, "orgLevel", "请求报文中未包含orgLevel");
        Assert.hasKeyAndValue(reqJson, "orgFlag", "请求报文中未包含orgFlag");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含组织描述");


        GovPartyOrgPo govPartyOrgPo = BeanConvertUtil.covertBean(reqJson, GovPartyOrgPo.class);
        return saveGovPartyOrgBMOImpl.save(govPartyOrgPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govPartyOrg/updateGovPartyOrg
     * @path /app/govPartyOrg/updateGovPartyOrg
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovPartyOrg", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovPartyOrg(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "orgName", "请求报文中未包含orgName");
        Assert.hasKeyAndValue(reqJson, "preOrgId", "请求报文中未包含preOrgId");
        Assert.hasKeyAndValue(reqJson, "preOrgName", "请求报文中未包含preOrgName");
        Assert.hasKeyAndValue(reqJson, "orgCode", "请求报文中未包含orgCode");
        Assert.hasKeyAndValue(reqJson, "orgSimpleName", "请求报文中未包含orgSimpleName");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personTel", "请求报文中未包含personTel");
        Assert.hasKeyAndValue(reqJson, "address", "请求报文中未包含address");
        Assert.hasKeyAndValue(reqJson, "orgLevel", "请求报文中未包含orgLevel");
        Assert.hasKeyAndValue(reqJson, "orgFlag", "请求报文中未包含orgFlag");
        Assert.hasKeyAndValue(reqJson, "orgId", "orgId不能为空");


        GovPartyOrgPo govPartyOrgPo = BeanConvertUtil.covertBean(reqJson, GovPartyOrgPo.class);
        return updateGovPartyOrgBMOImpl.update(govPartyOrgPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govPartyOrg/deleteGovPartyOrg
     * @path /app/govPartyOrg/deleteGovPartyOrg
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovPartyOrg", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovPartyOrg(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "orgId", "orgId不能为空");


        GovPartyOrgPo govPartyOrgPo = BeanConvertUtil.covertBean(reqJson, GovPartyOrgPo.class);
        return deleteGovPartyOrgBMOImpl.delete(govPartyOrgPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govPartyOrg/queryGovPartyOrg
     * @path /app/govPartyOrg/queryGovPartyOrg
     * @param orgId ID
     * @return
     */
    @RequestMapping(value = "/queryGovPartyOrg", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovPartyOrg(@RequestParam(value = "orgId" , required = false) String orgId,
                                                      @RequestParam(value = "preOrgId" , required = false) String preOrgId,
                                                      @RequestParam(value = "personName" , required = false) String personName,
                                                      @RequestParam(value = "orgLevel" , required = false) String orgLevel,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovPartyOrgDto govPartyOrgDto = new GovPartyOrgDto();
        govPartyOrgDto.setPage(page);
        govPartyOrgDto.setRow(row);
        govPartyOrgDto.setOrgId(orgId);
        govPartyOrgDto.setPreOrgId(preOrgId);
        govPartyOrgDto.setPersonName(personName);
        govPartyOrgDto.setOrgLevel(orgLevel);
        return getGovPartyOrgBMOImpl.get(govPartyOrgDto);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govPartyOrg/queryGovPartyOrg
     * @path /app/govPartyOrg/queryGovPartyOrg
     * @param orgId ID
     * @return
     */
    @RequestMapping(value = "/queryNotGovPartyOrg", method = RequestMethod.GET)
    public ResponseEntity<String> queryNotGovPartyOrg(@RequestParam(value = "orgId" , required = false) String orgId,
                                                   @RequestParam(value = "preOrgId" , required = false) String preOrgId,
                                                   @RequestParam(value = "personName" , required = false) String personName,
                                                   @RequestParam(value = "orgLevel" , required = false) String orgLevel,
                                                   @RequestParam(value = "page") int page,
                                                   @RequestParam(value = "row") int row) {
        GovPartyOrgDto govPartyOrgDto = new GovPartyOrgDto();
        govPartyOrgDto.setPage(page);
        govPartyOrgDto.setRow(row);
        govPartyOrgDto.setOrgId(orgId);
        govPartyOrgDto.setPreOrgId(preOrgId);
        govPartyOrgDto.setPersonName(personName);
        govPartyOrgDto.setOrgLevel(orgLevel);
        return getGovPartyOrgBMOImpl.queryNotGovPartyOrg(govPartyOrgDto);
    }
}
