package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 党内职务组件内部之间使用，没有给外围系统提供服务能力
 * 党内职务服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovWorkTypeServiceDao {


    /**
     * 保存 党内职务信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovWorkTypeInfo(Map info) throws DAOException;




    /**
     * 查询党内职务信息（instance过程）
     * 根据bId 查询党内职务信息
     * @param info bId 信息
     * @return 党内职务信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovWorkTypeInfo(Map info) throws DAOException;



    /**
     * 修改党内职务信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovWorkTypeInfo(Map info) throws DAOException;


    /**
     * 查询党内职务总数
     *
     * @param info 党内职务信息
     * @return 党内职务数量
     */
    int queryGovWorkTypesCount(Map info);

}
