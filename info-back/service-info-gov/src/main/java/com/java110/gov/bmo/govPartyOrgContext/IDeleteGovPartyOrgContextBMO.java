package com.java110.gov.bmo.govPartyOrgContext;
import com.java110.po.govPartyOrgContext.GovPartyOrgContextPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovPartyOrgContextBMO {


    /**
     * 修改党组织简介
     * add by wuxw
     * @param govPartyOrgContextPo
     * @return
     */
    ResponseEntity<String> delete(GovPartyOrgContextPo govPartyOrgContextPo);


}
