package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govPartyOrgContext.GovPartyOrgContextDto;
import com.java110.gov.bmo.govPartyOrgContext.IDeleteGovPartyOrgContextBMO;
import com.java110.gov.bmo.govPartyOrgContext.IGetGovPartyOrgContextBMO;
import com.java110.gov.bmo.govPartyOrgContext.ISaveGovPartyOrgContextBMO;
import com.java110.gov.bmo.govPartyOrgContext.IUpdateGovPartyOrgContextBMO;
import com.java110.po.govPartyOrgContext.GovPartyOrgContextPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govPartyOrgContext")
public class GovPartyOrgContextApi {

    @Autowired
    private ISaveGovPartyOrgContextBMO saveGovPartyOrgContextBMOImpl;
    @Autowired
    private IUpdateGovPartyOrgContextBMO updateGovPartyOrgContextBMOImpl;
    @Autowired
    private IDeleteGovPartyOrgContextBMO deleteGovPartyOrgContextBMOImpl;

    @Autowired
    private IGetGovPartyOrgContextBMO getGovPartyOrgContextBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govPartyOrgContext/saveGovPartyOrgContext
     * @path /app/govPartyOrgContext/saveGovPartyOrgContext
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovPartyOrgContext", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovPartyOrgContext(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");


        GovPartyOrgContextPo govPartyOrgContextPo = BeanConvertUtil.covertBean(reqJson, GovPartyOrgContextPo.class);
        return saveGovPartyOrgContextBMOImpl.save(govPartyOrgContextPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govPartyOrgContext/updateGovPartyOrgContext
     * @path /app/govPartyOrgContext/updateGovPartyOrgContext
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovPartyOrgContext", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovPartyOrgContext(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "orgId", "orgId不能为空");


        GovPartyOrgContextPo govPartyOrgContextPo = BeanConvertUtil.covertBean(reqJson, GovPartyOrgContextPo.class);
        return updateGovPartyOrgContextBMOImpl.update(govPartyOrgContextPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govPartyOrgContext/deleteGovPartyOrgContext
     * @path /app/govPartyOrgContext/deleteGovPartyOrgContext
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovPartyOrgContext", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovPartyOrgContext(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "orgId", "orgId不能为空");


        GovPartyOrgContextPo govPartyOrgContextPo = BeanConvertUtil.covertBean(reqJson, GovPartyOrgContextPo.class);
        return deleteGovPartyOrgContextBMOImpl.delete(govPartyOrgContextPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govPartyOrgContext/queryGovPartyOrgContext
     * @path /app/govPartyOrgContext/queryGovPartyOrgContext
     * @param
     * @return
     */
    @RequestMapping(value = "/queryGovPartyOrgContext", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovPartyOrgContext(
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovPartyOrgContextDto govPartyOrgContextDto = new GovPartyOrgContextDto();
        govPartyOrgContextDto.setPage(page);
        govPartyOrgContextDto.setRow(row);
        return getGovPartyOrgContextBMOImpl.get(govPartyOrgContextDto);
    }
}
