package com.java110.gov.bmo.govPartyMemberChange;
import com.java110.po.govPartyMemberChange.GovPartyMemberChangePo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovPartyMemberChangeBMO {


    /**
     * 修改党关系管理
     * add by wuxw
     * @param govPartyMemberChangePo
     * @return
     */
    ResponseEntity<String> update(GovPartyMemberChangePo govPartyMemberChangePo);


}
