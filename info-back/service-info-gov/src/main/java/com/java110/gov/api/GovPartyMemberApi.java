package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govPartyMember.GovPartyMemberDto;
import com.java110.gov.bmo.govPartyMember.IDeleteGovPartyMemberBMO;
import com.java110.gov.bmo.govPartyMember.IGetGovPartyMemberBMO;
import com.java110.gov.bmo.govPartyMember.ISaveGovPartyMemberBMO;
import com.java110.gov.bmo.govPartyMember.IUpdateGovPartyMemberBMO;
import com.java110.po.govPartyMember.GovPartyMemberPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govPartyMember")
public class GovPartyMemberApi {

    @Autowired
    private ISaveGovPartyMemberBMO saveGovPartyMemberBMOImpl;
    @Autowired
    private IUpdateGovPartyMemberBMO updateGovPartyMemberBMOImpl;
    @Autowired
    private IDeleteGovPartyMemberBMO deleteGovPartyMemberBMOImpl;

    @Autowired
    private IGetGovPartyMemberBMO getGovPartyMemberBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govPartyMember/saveGovPartyMember
     * @path /app/govPartyMember/saveGovPartyMember
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovPartyMember", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovPartyMember(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "personId", "请求报文中未包含personId");
        Assert.hasKeyAndValue(reqJson, "orgId", "请求报文中未包含orgId");
        Assert.hasKeyAndValue(reqJson, "orgName", "请求报文中未包含orgName");
        Assert.hasKeyAndValue(reqJson, "govTypeId", "请求报文中未包含govTypeId");
        Assert.hasKeyAndValue(reqJson, "memberFlag", "请求报文中未包含memberFlag");


        GovPartyMemberPo govPartyMemberPo = BeanConvertUtil.covertBean(reqJson, GovPartyMemberPo.class);
        return saveGovPartyMemberBMOImpl.save(govPartyMemberPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govPartyMember/updateGovPartyMember
     * @path /app/govPartyMember/updateGovPartyMember
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovPartyMember", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovPartyMember(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "personId", "请求报文中未包含personId");
        Assert.hasKeyAndValue(reqJson, "orgId", "请求报文中未包含orgId");
        Assert.hasKeyAndValue(reqJson, "orgName", "请求报文中未包含orgName");
        Assert.hasKeyAndValue(reqJson, "govTypeId", "请求报文中未包含govTypeId");
        Assert.hasKeyAndValue(reqJson, "memberFlag", "请求报文中未包含memberFlag");
        Assert.hasKeyAndValue(reqJson, "govMemberId", "govMemberId不能为空");


        GovPartyMemberPo govPartyMemberPo = BeanConvertUtil.covertBean(reqJson, GovPartyMemberPo.class);
        return updateGovPartyMemberBMOImpl.update(govPartyMemberPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govPartyMember/deleteGovPartyMember
     * @path /app/govPartyMember/deleteGovPartyMember
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovPartyMember", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovPartyMember(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govMemberId", "govMemberId不能为空");


        GovPartyMemberPo govPartyMemberPo = BeanConvertUtil.covertBean(reqJson, GovPartyMemberPo.class);
        return deleteGovPartyMemberBMOImpl.delete(govPartyMemberPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govPartyMember/queryGovPartyMember
     * @path /app/govPartyMember/queryGovPartyMember
     * @param govTypeId
     * @return
     */
    @RequestMapping(value = "/queryGovPartyMember", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovPartyMember(@RequestParam(value = "govTypeId" , required = false) String govTypeId,
                                                      @RequestParam(value = "caId" , required = false) String caId,
                                                      @RequestParam(value = "personId" , required = false) String personId,
                                                      @RequestParam(value = "orgId" , required = false) String orgId,
                                                      @RequestParam(value = "memberFlag" , required = false) String memberFlag,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovPartyMemberDto govPartyMemberDto = new GovPartyMemberDto();
        govPartyMemberDto.setPage(page);
        govPartyMemberDto.setRow(row);
        govPartyMemberDto.setGovTypeId(govTypeId);
        govPartyMemberDto.setCaId(caId);
        govPartyMemberDto.setPersonId(personId);
        govPartyMemberDto.setOrgId(orgId);
        govPartyMemberDto.setMemberFlag(memberFlag);
        return getGovPartyMemberBMOImpl.get(govPartyMemberDto);
    }
}
