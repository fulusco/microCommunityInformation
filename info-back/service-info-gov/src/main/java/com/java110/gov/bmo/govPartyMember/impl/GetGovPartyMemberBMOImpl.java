package com.java110.gov.bmo.govPartyMember.impl;

import com.java110.gov.bmo.govPartyMember.IGetGovPartyMemberBMO;
import com.java110.intf.gov.IGovPartyMemberInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govPartyMember.GovPartyMemberDto;

import java.util.ArrayList;
import java.util.List;

@Service("getGovPartyMemberBMOImpl")
public class GetGovPartyMemberBMOImpl implements IGetGovPartyMemberBMO {

    @Autowired
    private IGovPartyMemberInnerServiceSMO govPartyMemberInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govPartyMemberDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovPartyMemberDto govPartyMemberDto) {


        int count = govPartyMemberInnerServiceSMOImpl.queryGovPartyMembersCount(govPartyMemberDto);

        List<GovPartyMemberDto> govPartyMemberDtos = null;
        if (count > 0) {
            govPartyMemberDtos = govPartyMemberInnerServiceSMOImpl.queryGovPartyMembers(govPartyMemberDto);
        } else {
            govPartyMemberDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPartyMemberDto.getRow()), count, govPartyMemberDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
