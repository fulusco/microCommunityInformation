package com.java110.gov.bmo.govPartyOrg.impl;


import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govPartyOrg.IUpdateGovPartyOrgBMO;
import com.java110.intf.gov.IGovPartyOrgContextInnerServiceSMO;
import com.java110.intf.gov.IGovPartyOrgInnerServiceSMO;
import com.java110.po.govPartyOrg.GovPartyOrgPo;
import com.java110.po.govPartyOrgContext.GovPartyOrgContextPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovPartyOrgBMOImpl")
public class UpdateGovPartyOrgBMOImpl implements IUpdateGovPartyOrgBMO {

    @Autowired
    private IGovPartyOrgInnerServiceSMO govPartyOrgInnerServiceSMOImpl;
    @Autowired
    private IGovPartyOrgContextInnerServiceSMO govPartyOrgContextInnerServiceSMOImpl;
    /**
     *
     *
     * @param govPartyOrgPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovPartyOrgPo govPartyOrgPo) {

        int flag = govPartyOrgInnerServiceSMOImpl.updateGovPartyOrg(govPartyOrgPo);

        if (flag > 0) {
            GovPartyOrgContextPo govPartyOrgContextPo = new GovPartyOrgContextPo();
            govPartyOrgContextPo.setContext( govPartyOrgPo.getContext() );
            govPartyOrgContextPo.setOrgId( govPartyOrgPo.getOrgId() );
            govPartyOrgContextInnerServiceSMOImpl.updateGovPartyOrgContext( govPartyOrgContextPo );
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
