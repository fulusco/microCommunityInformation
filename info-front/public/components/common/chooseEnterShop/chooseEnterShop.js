(function(vc){
    let DEFAULT_PAGE = 1;
    let DEFAULT_ROW = 10;
    vc.extends({
        data:{
            navShopInfo: {
                _currentShop: {},
                shopInfos: [],
                shopInfo: [],
                errorInfo: '',
                searchShopName: '',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseEnterShop','openChooseEnterShopModel',function(_param){
                $('#chooseEnterShopModel').modal('show');
                $that.navShopInfo.searchShopName = '';
                $that.listEnterShop(DEFAULT_PAGE, DEFAULT_ROW);
            });
            vc.on('chooseEnterShop','paginationPlus', 'page_event', function (_currentPage) {
                vc.component.listEnterShop(_currentPage, DEFAULT_ROW);
            });
        },
        methods:{
            listEnterShop: function (_page, _row) {
                var param = {
                    params: {
                        _uid: '123mlkdinkldldijdhuudjdjkkd',
                        page: _page,
                        row: _row,
                        state:"002",
                        shopName: $that.navShopInfo.searchShopName
                    }
                };
                vc.http.apiGet('/shop/queryShop',
                    param,
                    function (json, res) {
                        if (res.status == 200) {
                            let _data = JSON.parse(json);
                            $that.navShopInfo.shopInfo = _data.data;
                            vc.emit('chooseEnterShop','paginationPlus', 'init', {
                                total: _data.records,
                                currentPage: _page
                            });
                        }
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _chooseCurrentShop: function (_currentShop) {
                vc.setCurrentCommunity(_currentShop);
                //vm.navShopInfo._currentShop = _currentShop;
                //中心加载当前页
                location.reload();
            },
            _queryEnterShop: function () {
                $that.listEnterShop(DEFAULT_PAGE, DEFAULT_ROW)
            }
        }

    });
})(window.vc);
