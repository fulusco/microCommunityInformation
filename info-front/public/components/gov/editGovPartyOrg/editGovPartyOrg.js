(function (vc, vm) {

    vc.extends({
        data: {
            editGovPartyOrgInfo: {
                orgId: '',
                orgName: '',
                preOrgId: '',
                preOrgName: '',
                orgCode: '',
                orgSimpleName: '',
                personName: '',
                personTel: '',
                address: '',
                orgLevel: '',
                orgFlag: '',
                context: '',
                communityOrArea: '',
                ramark: '',
                listGovPartyOrgs: [],
                communityOrAreas: []
                
            }
        },
        _initMethod: function () {
            $that._initEditGovPartyOrg();
        },
        _initEvent: function () {
            vc.on('editGovPartyOrg', 'openEditGovPartyOrgModal', function (_params) {
                vc.component.refreshEditGovPartyOrgInfo();
                $('#editGovPartyOrgModel').modal('show');
                vc.copyObject(_params, vc.component.editGovPartyOrgInfo);
                $(".editPartOrgSummernote").summernote('code', vc.component.editGovPartyOrgInfo.context);
                $that._listEditGovPartyOrgs($that.editGovPartyOrgInfo.orgId);
                $that._listEditCommunityOrArea($that.editGovPartyOrgInfo.orgLevel);
            });
        },
        methods: {
            editGovPartyOrgValidate: function () {
                return vc.validate.validate({
                    editGovPartyOrgInfo: vc.component.editGovPartyOrgInfo
                }, {
                    'editGovPartyOrgInfo.orgName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "组织名称超长"
                        },
                    ],
                    'editGovPartyOrgInfo.preOrgId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "上级组织不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "上级组织ID超长"
                        },
                    ],
                    'editGovPartyOrgInfo.preOrgName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "上级组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "上级组织名称超长"
                        },
                    ],
                    'editGovPartyOrgInfo.orgCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "组织编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "组织编号超长"
                        },
                    ],
                    'editGovPartyOrgInfo.orgSimpleName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "名称简称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "32",
                            errInfo: "名称简称超长"
                        },
                    ],
                    'editGovPartyOrgInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "负责人超长"
                        },
                    ],
                    'editGovPartyOrgInfo.personTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "负责电话格式错误"
                        },
                    ],
                    'editGovPartyOrgInfo.address': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "地址超长"
                        },
                    ],
                    'editGovPartyOrgInfo.orgLevel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党组织级别不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "党组织级别超长"
                        },
                    ],
                    'editGovPartyOrgInfo.orgFlag': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否建立党组织不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "是否建立党组织超长"
                        },
                    ],
                    'editGovPartyOrgInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],
                    'editGovPartyOrgInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        }
                    ],
                    'editGovPartyOrgInfo.orgId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党组织ID不能为空"
                        }]

                });
            },
            editGovPartyOrg: function () {
                if (!vc.component.editGovPartyOrgValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govPartyOrg/updateGovPartyOrg',
                    JSON.stringify(vc.component.editGovPartyOrgInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovPartyOrgModel').modal('hide');
                            vc.emit('govPartyOrgManage', 'listGovPartyOrg', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _closeEditGovPartyOrgModel: function () {
                $that.refreshEditGovPartyOrgInfo();
                vc.emit('govPartyOrgManage', 'listGovPartyOrg', {});
            },
            _listEditGovPartyOrgs: function (_orgId) {
                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        preOrgId: '-1',
                        orgId: _orgId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPartyOrg/queryNotGovPartyOrg',
                    param,
                    function (json, res) {
                        var _govPartyOrgManageInfo = JSON.parse(json);
                        vc.component.editGovPartyOrgInfo.total = _govPartyOrgManageInfo.total;
                        vc.component.editGovPartyOrgInfo.records = _govPartyOrgManageInfo.records;
                        vc.component.editGovPartyOrgInfo.listGovPartyOrgs = _govPartyOrgManageInfo.data;
                      
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listEditCommunityOrArea: function(_orgLevel){
                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                if('1001' == _orgLevel){
                    //发送get请求
                    vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govPartyOrgManageInfo = JSON.parse(json);
                        vc.component.editGovPartyOrgInfo.total = _govPartyOrgManageInfo.total;
                        vc.component.editGovPartyOrgInfo.records = _govPartyOrgManageInfo.records;
                        vc.component.editGovPartyOrgInfo.communityOrAreas = _govPartyOrgManageInfo.data;
                    
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                    );
                    $that.editGovPartyOrgInfo.preOrgId='-1';
                    $that.editGovPartyOrgInfo.preOrgName='无';
                }else if('2002' == _orgLevel){
                    //发送get请求
                    vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govPartyOrgManageInfo = JSON.parse(json);
                        vc.component.editGovPartyOrgInfo.total = _govPartyOrgManageInfo.total;
                        vc.component.editGovPartyOrgInfo.records = _govPartyOrgManageInfo.records;
                        vc.component.editGovPartyOrgInfo.communityOrAreas = _govPartyOrgManageInfo.data;
                    
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                    );
                }
            },
            _setEditPreOrgName:function(_preOrgId){
                $that.editGovPartyOrgInfo.listGovPartyOrgs.forEach(item => {
                    if (item.orgId == _preOrgId) {
                        $that.editGovPartyOrgInfo.preOrgName = item.orgName;
                    }
                });
            },
            _initEditGovPartyOrg: function () {
                let $summernote = $('.editPartOrgSummernote').summernote({
                    lang: 'zh-CN',
                    height: 300,
                    placeholder: '必填，请输入党组织简介',
                    callbacks: {
                        onImageUpload: function (files, editor, $editable) {
                            $that.sendEditFile($summernote, files);
                        },
                        onChange: function (contexts, $editable) {
                            $that.editGovPartyOrgInfo.context = contexts;
                        }
                    },
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                    ],
                });
            },
            sendEditFile: function ($summernote, files) {
                console.log('上传图片', files);

                var param = new FormData();
                param.append("uploadFile", files[0]);
                //param.append('shopId', vc.getCurrentCommunity().shopId);

                vc.http.upload(
                    'addNoticeView',
                    'uploadImage',
                    param,
                    {
                        emulateJSON: true,
                        //添加请求头
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        if (res.status == 200) {
                            var data = JSON.parse(json);
                            //关闭model
                            $summernote.summernote('insertImage',  data.fileId );
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });

            },
            refreshEditGovPartyOrgInfo: function () {
                let _listGovPartyOrgs = $that.editGovPartyOrgInfo.listGovPartyOrgs;
                let _communityOrAreas = $that.editGovPartyOrgInfo.communityOrAreas;
                vc.component.editGovPartyOrgInfo = {
                    orgId: '',
                    orgName: '',
                    preOrgId: '',
                    preOrgName: '',
                    orgCode: '',
                    orgSimpleName: '',
                    personName: '',
                    personTel: '',
                    address: '',
                    orgLevel: '',
                    orgFlag: '',
                    context: '',
                    communityOrArea: '',
                    ramark: '',
                    listGovPartyOrgs: _listGovPartyOrgs,
                    communityOrAreas: _communityOrAreas
                }
            }
        }
    });

})(window.vc, window.vc.component);
