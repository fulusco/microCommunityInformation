(function (vc, vm) {

    vc.extends({
        data: {
            editGovPartyMemberInfo: {
                govMemberId: '',
                caId: '',
                personId: '',
                personName:'',
                orgId: '',
                orgName: '',
                govTypeId: '',
                memberFlag: '',
                ramark: '',
                govCommunityAreas: [],
                govWorkTypes: [],
                govPartyOrgs: []
            }
        },
        _initMethod: function () {
            $that._listEditGovCommunityAreas();
            $that._listEditGovWorkTypes();
            $that._listEditGovPartyOrgs();
        },
        _initEvent: function () {
            vc.on('editGovPartyMember', 'openEditGovPartyMemberModal', function (_params) {
                vc.component.refreshEditGovPartyMemberInfo();
                $('#editGovPartyMemberModel').modal('show');
                vc.copyObject(_params, vc.component.editGovPartyMemberInfo);
            });
            vc.on('openChooseGovPerson', 'chooseGovPerson', function (_param) {
                console.log(_param);
                $that.editGovPartyMemberInfo.personId = _param.govPersonId;
                $that.editGovPartyMemberInfo.personName = _param.personName;
            });
        },
        methods: {
            editGovPartyMemberValidate: function () {
                return vc.validate.validate({
                    editGovPartyMemberInfo: vc.component.editGovPartyMemberInfo
                }, {
                    'editGovPartyMemberInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属区域超长"
                        },
                    ],
                    'editGovPartyMemberInfo.personId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党员名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "党员名称超长"
                        },
                    ],
                    'editGovPartyMemberInfo.orgId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "组织名称超长"
                        },
                    ],
                    'editGovPartyMemberInfo.orgName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "组织名称超长"
                        },
                    ],
                    'editGovPartyMemberInfo.govTypeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党内职务不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "党内职务超长"
                        },
                    ],
                    'editGovPartyMemberInfo.memberFlag': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否优秀不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "是否优秀超长"
                        },
                    ],
                    'editGovPartyMemberInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],
                    'editGovPartyMemberInfo.govMemberId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党员管理ID不能为空"
                        }]

                });
            },
            editGovPartyMember: function () {
                if (!vc.component.editGovPartyMemberValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govPartyMember/updateGovPartyMember',
                    JSON.stringify(vc.component.editGovPartyMemberInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovPartyMemberModel').modal('hide');
                            vc.emit('govPartyMemberManage', 'listGovPartyMember', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.editGovPartyMemberInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.editGovPartyMemberInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.editGovPartyMemberInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listEditGovWorkTypes: function (_page, _rows) {


                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govWorkType/queryGovWorkType',
                    param,
                    function (json, res) {
                        var _govWorkTypeManageInfo = JSON.parse(json);
                        vc.component.editGovPartyMemberInfo.total = _govWorkTypeManageInfo.total;
                        vc.component.editGovPartyMemberInfo.records = _govWorkTypeManageInfo.records;
                        vc.component.editGovPartyMemberInfo.govWorkTypes = _govWorkTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listEditGovPartyOrgs: function (_page, _rows) {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPartyOrg/queryGovPartyOrg',
                    param,
                    function (json, res) {
                        var _govPartyOrgManageInfo = JSON.parse(json);
                        vc.component.editGovPartyMemberInfo.total = _govPartyOrgManageInfo.total;
                        vc.component.editGovPartyMemberInfo.records = _govPartyOrgManageInfo.records;
                        vc.component.editGovPartyMemberInfo.govPartyOrgs = _govPartyOrgManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _loadEditGovPersonInfo: function (_govPersonId) {
                var param = {
                    params: {
                        page: 1,
                        row: 1,
                        govPersonId: _govPersonId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPerson/queryGovPerson',
                    param,
                    function (json) {
                        var _govPersonInfo = JSON.parse(json);
                        console.log(_govPersonInfo);
                        $that.editGovPartyMemberInfo.personName = _govPersonInfo.data[0].personName;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _setEditPartyOrgName : function(_orgId){
                $that.editGovPartyMemberInfo.govPartyOrgs.forEach(item => {
                    if (item.orgId == _orgId) {
                        $that.editGovPartyMemberInfo.orgName = item.orgName;
                    }
                });
            },
            _closeEditGovPartyMember: function () {
                $that.refreshEditGovPartyMemberInfo();
                vc.emit('govPartyMemberManage', 'listGovPartyMember', {});
            },
            _openEditChooseGovPersonModel: function (_caId) {
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit( 'chooseGovPerson', 'openChooseGovPersonModel', _caId);
            },
            refreshEditGovPartyMemberInfo: function () {
                let _govCommunityAreas =  vc.component.editGovPartyMemberInfo.govCommunityAreas;
                let _govWorkTypes =  vc.component.editGovPartyMemberInfo.govWorkTypes;
                let _govPartyOrgs =  vc.component.editGovPartyMemberInfo.govPartyOrgs;
                vc.component.editGovPartyMemberInfo = {
                    govMemberId: '',
                    caId: '',
                    personId: '',
                    personName:'',
                    orgId: '',
                    orgName: '',
                    govTypeId: '',
                    memberFlag: '',
                    ramark: '',
                    govCommunityAreas: _govCommunityAreas,
                    govWorkTypes: _govWorkTypes,
                    govPartyOrgs: _govPartyOrgs
                }
            }
        }
    });

})(window.vc, window.vc.component);
