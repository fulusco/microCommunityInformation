(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovBuildingTypeInfo: {
                typeId: '',
                typeName: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovBuildingType', 'openAddGovBuildingTypeModal', function () {
                $('#addGovBuildingTypeModel').modal('show');
            });
        },
        methods: {
            addGovBuildingTypeValidate() {
                return vc.validate.validate({
                    addGovBuildingTypeInfo: vc.component.addGovBuildingTypeInfo
                }, {
                    'addGovBuildingTypeInfo.typeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "建筑分类名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "建筑分类名称超长"
                        },
                    ],
                    'addGovBuildingTypeInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "描述不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "描述太长"
                        },
                    ],




                });
            },
            saveGovBuildingTypeInfo: function () {
                if (!vc.component.addGovBuildingTypeValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                //vc.component.addGovBuildingTypeInfo.communityId = vc.getCurrentCommunity().communityId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovBuildingTypeInfo);
                    $('#addGovBuildingTypeModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govBuildingType/saveGovBuildingType',
                    JSON.stringify(vc.component.addGovBuildingTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovBuildingTypeModel').modal('hide');
                            vc.component.clearAddGovBuildingTypeInfo();
                            vc.emit('govBuildingTypeManage', 'listGovBuildingType', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovBuildingTypeInfo: function () {
                vc.component.addGovBuildingTypeInfo = {
                    typeName: '',
                    ramark: '',

                };
            }
        }
    });

})(window.vc);
