(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovPartyMemberInfo: {
                govMemberId: '',
                caId: '',
                personId: '',
                _personName:'',
                orgId: '',
                orgName: '',
                govTypeId: '',
                memberFlag: '',
                ramark: '',
                govCommunityAreas: [],
                govWorkTypes: [],
                govPartyOrgs: []
            }
        },
        _initMethod: function () {
            $that._listAddGovCommunityAreas();
            $that._listAddGovWorkTypes();
            $that._listAddGovPartyOrgs();
        },
        _initEvent: function () {
            vc.on('addGovPartyMember', 'openAddGovPartyMemberModal', function () {
                $('#addGovPartyMemberModel').modal('show');
            });
            vc.on('openChooseGovPerson', 'chooseGovPerson', function (_param) {
                $that.addGovPartyMemberInfo.personId = _param.govPersonId;
                $that.addGovPartyMemberInfo._personName = _param.personName;
            });
        },
        methods: {
            addGovPartyMemberValidate() {
                return vc.validate.validate({
                    addGovPartyMemberInfo: vc.component.addGovPartyMemberInfo
                }, {
                    'addGovPartyMemberInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属区域超长"
                        },
                    ],
                    'addGovPartyMemberInfo.personId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党员名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "党员名称超长"
                        },
                    ],
                    'addGovPartyMemberInfo.orgId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "组织名称超长"
                        },
                    ],
                    'addGovPartyMemberInfo.orgName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "组织名称超长"
                        },
                    ],
                    'addGovPartyMemberInfo.govTypeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党内职务不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "党内职务超长"
                        },
                    ],
                    'addGovPartyMemberInfo.memberFlag': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否优秀不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "是否优秀超长"
                        },
                    ],
                    'addGovPartyMemberInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],




                });
            },
            saveGovPartyMemberInfo: function () {
                if (!vc.component.addGovPartyMemberValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovPartyMemberInfo);
                    $('#addGovPartyMemberModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govPartyMember/saveGovPartyMember',
                    JSON.stringify(vc.component.addGovPartyMemberInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovPartyMemberModel').modal('hide');
                            vc.component.clearAddGovPartyMemberInfo();
                            vc.emit('govPartyMemberManage', 'listGovPartyMember', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _listAddGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.addGovPartyMemberInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.addGovPartyMemberInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.addGovPartyMemberInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listAddGovWorkTypes: function (_page, _rows) {


                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govWorkType/queryGovWorkType',
                    param,
                    function (json, res) {
                        var _govWorkTypeManageInfo = JSON.parse(json);
                        vc.component.addGovPartyMemberInfo.total = _govWorkTypeManageInfo.total;
                        vc.component.addGovPartyMemberInfo.records = _govWorkTypeManageInfo.records;
                        vc.component.addGovPartyMemberInfo.govWorkTypes = _govWorkTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listAddGovPartyOrgs: function (_page, _rows) {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPartyOrg/queryGovPartyOrg',
                    param,
                    function (json, res) {
                        var _govPartyOrgManageInfo = JSON.parse(json);
                        vc.component.addGovPartyMemberInfo.total = _govPartyOrgManageInfo.total;
                        vc.component.addGovPartyMemberInfo.records = _govPartyOrgManageInfo.records;
                        vc.component.addGovPartyMemberInfo.govPartyOrgs = _govPartyOrgManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddChooseGovPersonModel: function (_caId) {
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit( 'chooseGovPerson', 'openChooseGovPersonModel', _caId);
            },
            _closeAddGovPartyMember: function () {
                $that.clearAddGovPartyMemberInfo();
                vc.emit('govPartyMemberManage', 'listGovPartyMember', {});
            },
            _setAddPartyOrgName : function(_orgId){
                $that.addGovPartyMemberInfo.govPartyOrgs.forEach(item => {
                    if (item.orgId == _orgId) {
                        $that.addGovPartyMemberInfo.orgName = item.orgName;
                    }
                });
            },
            clearAddGovPartyMemberInfo: function () {
                vc.component.addGovPartyMemberInfo = {
                    caId: '',
                    personId: '',
                    _personName: '',
                    orgId: '',
                    orgName: '',
                    govTypeId: '',
                    memberFlag: '',
                    ramark: '',
                    govCommunityAreas: [],
                    govWorkTypes: [],
                    govPartyOrgs: []
                };
            }
        }
    });

})(window.vc);
