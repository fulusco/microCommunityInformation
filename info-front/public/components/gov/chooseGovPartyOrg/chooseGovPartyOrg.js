(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovPartyOrg:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovPartyOrgInfo:{
                govPartyOrgs:[],
                _currentGovPartyOrgName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovPartyOrg','openChooseGovPartyOrgModel',function(_param){
                $('#chooseGovPartyOrgModel').modal('show');
                vc.component._refreshChooseGovPartyOrgInfo();
                vc.component._loadAllGovPartyOrgInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovPartyOrgInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govPartyOrg.listGovPartyOrgs',
                             param,
                             function(json){
                                var _govPartyOrgInfo = JSON.parse(json);
                                vc.component.chooseGovPartyOrgInfo.govPartyOrgs = _govPartyOrgInfo.govPartyOrgs;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovPartyOrg:function(_govPartyOrg){
                if(_govPartyOrg.hasOwnProperty('name')){
                     _govPartyOrg.govPartyOrgName = _govPartyOrg.name;
                }
                vc.emit($props.emitChooseGovPartyOrg,'chooseGovPartyOrg',_govPartyOrg);
                vc.emit($props.emitLoadData,'listGovPartyOrgData',{
                    govPartyOrgId:_govPartyOrg.govPartyOrgId
                });
                $('#chooseGovPartyOrgModel').modal('hide');
            },
            queryGovPartyOrgs:function(){
                vc.component._loadAllGovPartyOrgInfo(1,10,vc.component.chooseGovPartyOrgInfo._currentGovPartyOrgName);
            },
            _refreshChooseGovPartyOrgInfo:function(){
                vc.component.chooseGovPartyOrgInfo._currentGovPartyOrgName = "";
            }
        }

    });
})(window.vc);
