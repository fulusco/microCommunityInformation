(function (vc) {
    vc.extends({
        propTypes: {
            emitChooseGovPartyMember: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovPartyMemberInfo: {
                govPartyMembers: [],
                _currentGovPartyMemberName: '',
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovPartyMember', 'openChooseGovPartyMemberModel', function (_param) {
                $('#chooseGovPartyMemberModel').modal('show');
                vc.component._refreshChooseGovPartyMemberInfo();
                vc.component._loadAllGovPartyMemberInfo(1, 10, _param);
            });
        },
        methods: {
            _loadAllGovPartyMemberInfo: function (_page, _row, _caId) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        caId: _caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPartyMember/queryGovPartyMember',
                    param,
                    function (json) {
                        var _govPartyMemberInfo = JSON.parse(json);
                        vc.component.chooseGovPartyMemberInfo.govPartyMembers = _govPartyMemberInfo.data;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovPartyMember: function (_govPartyMember) {

                vc.emit($props.emitChooseGovPartyMember, 'chooseGovPartyMember', _govPartyMember);
                $('#chooseGovPartyMemberModel').modal('hide');
            },
            queryGovPartyMembers: function () {
                vc.component._loadAllGovPartyMemberInfo(1, 10, vc.component.chooseGovPartyMemberInfo._currentGovPartyMemberName);
            },
            _refreshChooseGovPartyMemberInfo: function () {
                vc.component.chooseGovPartyMemberInfo._currentGovPartyMemberName = "";
            }
        }

    });
})(window.vc);
