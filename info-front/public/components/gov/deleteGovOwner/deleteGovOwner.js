(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovOwnerInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovOwner', 'openDeleteGovOwnerModal', function (_params) {

                vc.component.deleteGovOwnerInfo = _params;
                $('#deleteGovOwnerModel').modal('show');

            });
        },
        methods: {
            deleteGovOwner: function () {
                vc.http.apiPost(
                    '/govOwner/deleteGovOwner',
                    JSON.stringify(vc.component.deleteGovOwnerInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovOwnerModel').modal('hide');
                            vc.emit('govOwnerManage', 'listGovOwner', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovOwnerModel: function () {
                $('#deleteGovOwnerModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
