(function (vc) {
    vc.extends({
        propTypes: {
            emitChooseGovFloor: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovFloorInfo: {
                govFloors: [],
                _currentGovFloorName: '',
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovFloor', 'openChooseGovFloorModel', function (_param) {
                $('#chooseGovFloorModel').modal('show');
                vc.component._refreshChooseGovFloorInfo();
                vc.component._loadAllGovFloorInfo(1, 10, '');
            });
        },
        methods: {
            _loadAllGovFloorInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        communityId: vc.getCurrentCommunity().communityId,
                        name: _name
                    }
                };

                //发送get请求
                vc.http.apiGet('govFloor.listGovFloors',
                    param,
                    function (json) {
                        var _govFloorInfo = JSON.parse(json);
                        vc.component.chooseGovFloorInfo.govFloors = _govFloorInfo.govFloors;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovFloor: function (_govFloor) {
                if (_govFloor.hasOwnProperty('name')) {
                    _govFloor.govFloorName = _govFloor.name;
                }
                vc.emit($props.emitChooseGovFloor, 'chooseGovFloor', _govFloor);
                vc.emit($props.emitLoadData, 'listGovFloorData', {
                    govFloorId: _govFloor.govFloorId
                });
                $('#chooseGovFloorModel').modal('hide');
            },
            queryGovFloors: function () {
                vc.component._loadAllGovFloorInfo(1, 10, vc.component.chooseGovFloorInfo._currentGovFloorName);
            },
            _refreshChooseGovFloorInfo: function () {
                vc.component.chooseGovFloorInfo._currentGovFloorName = "";
            }
        }

    });
})(window.vc);
