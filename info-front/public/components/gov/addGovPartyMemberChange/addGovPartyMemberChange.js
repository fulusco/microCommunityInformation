(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovPartyMemberChangeInfo: {
                govChangeId: '',
                caId: '',
                govMemberId: '',
                personName: '',
                srcOrgId: '',
                srcOrgName: '',
                targetOrgId: '',
                targetOrgName: '',
                chagneType: '',
                ramark: '',
                state: '1102',
                govPartyOrgs: [],
                govCommunityAreas: []

            }
        },
        _initMethod: function () {
            $that.addGovPartyMemberChangeInfo.chagneType = vc.getParam('chagneType');
            $that._listAddGovPartyOrgs();
            $that._listAddGovCommunityAreas();
        },
        _initEvent: function () {
            vc.on('addGovPartyMemberChange', 'openAddGovPartyMemberChangeModal', function (_chagneType) {
                $('#addGovPartyMemberChangeModel').modal('show');
            });
            vc.on('openChooseGovPartyMember', 'chooseGovPartyMember', function (_param) {
                $that.addGovPartyMemberChangeInfo.govMemberId = _param.govMemberId;
                $that.addGovPartyMemberChangeInfo.personName = _param.personName;
            });
        },
        methods: {
            addGovPartyMemberChangeValidate() {
                return vc.validate.validate({
                    addGovPartyMemberChangeInfo: vc.component.addGovPartyMemberChangeInfo
                }, {
                    'addGovPartyMemberChangeInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属区域超长"
                        },
                    ],
                    'addGovPartyMemberChangeInfo.govMemberId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党员名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "党员名称超长"
                        },
                    ],
                    'addGovPartyMemberChangeInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党员名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "党员名称超长"
                        },
                    ],
                    'addGovPartyMemberChangeInfo.srcOrgId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "源组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "源组织名称超长"
                        },
                    ],
                    'addGovPartyMemberChangeInfo.srcOrgName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "源组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "源组织名称超长"
                        },
                    ],
                    'addGovPartyMemberChangeInfo.targetOrgId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "目标组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "目标组织名称超长"
                        },
                    ],
                    'addGovPartyMemberChangeInfo.targetOrgName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "目标组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "目标组织名称超长"
                        },
                    ],
                    'addGovPartyMemberChangeInfo.chagneType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党关系类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "党关系类型超长"
                        },
                    ],
                    'addGovPartyMemberChangeInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],




                });
            },
            saveGovPartyMemberChangeInfo: function () {
                if (!vc.component.addGovPartyMemberChangeValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovPartyMemberChangeInfo);
                    $('#addGovPartyMemberChangeModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govPartyMemberChange/saveGovPartyMemberChange',
                    JSON.stringify(vc.component.addGovPartyMemberChangeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovPartyMemberChangeModel').modal('hide');
                            vc.component.clearAddGovPartyMemberChangeInfo();
                            vc.emit('govPartyMemberChangeManage', 'listGovPartyMemberChange', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _listAddGovPartyOrgs: function (_page, _rows) {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPartyOrg/queryGovPartyOrg',
                    param,
                    function (json, res) {
                        var _govPartyOrgManageInfo = JSON.parse(json);
                        vc.component.addGovPartyMemberChangeInfo.total = _govPartyOrgManageInfo.total;
                        vc.component.addGovPartyMemberChangeInfo.records = _govPartyOrgManageInfo.records;
                        vc.component.addGovPartyMemberChangeInfo.govPartyOrgs = _govPartyOrgManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listAddGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.addGovPartyMemberChangeInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.addGovPartyMemberChangeInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.addGovPartyMemberChangeInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _closeAddGovPartyMemberChange: function () {
                $that.clearAddGovPartyMemberChangeInfo();
                vc.emit('govPartyMemberChangeManage', 'listGovPartyMemberChange', {});
            },
            _openAddChooseGovPartyMember: function (_caId) {
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit('chooseGovPartyMember', 'openChooseGovPartyMemberModel', _caId);
            },
            _setAddPartySrcOrgName : function(_srcOrgId){
                $that.addGovPartyMemberChangeInfo.govPartyOrgs.forEach(item => {
                    if (item.orgId == _srcOrgId) {
                        $that.addGovPartyMemberChangeInfo.srcOrgName = item.orgName;
                    }
                });
            },
            _setAddPartyTargetOrgName : function(_targetOrgId){
                $that.addGovPartyMemberChangeInfo.govPartyOrgs.forEach(item => {
                    if (item.orgId == _targetOrgId) {
                        $that.addGovPartyMemberChangeInfo.targetOrgName = item.orgName;
                    }
                });
            },
            clearAddGovPartyMemberChangeInfo: function () {
                vc.component.addGovPartyMemberChangeInfo = {
                    caId: '',
                    govMemberId: '',
                    personName: '',
                    srcOrgId: '',
                    srcOrgName: '',
                    targetOrgId: '',
                    targetOrgName: '',
                    chagneType: '',
                    ramark: '',
                    state: '1102',
                    govPartyOrgs: [],
                    govCommunityAreas: []
                };
            }
        }
    });

})(window.vc);
