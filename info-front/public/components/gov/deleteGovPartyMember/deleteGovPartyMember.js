(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovPartyMemberInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovPartyMember', 'openDeleteGovPartyMemberModal', function (_params) {

                vc.component.deleteGovPartyMemberInfo = _params;
                $('#deleteGovPartyMemberModel').modal('show');

            });
        },
        methods: {
            deleteGovPartyMember: function () {
                vc.http.apiPost(
                    '/govPartyMember/deleteGovPartyMember',
                    JSON.stringify(vc.component.deleteGovPartyMemberInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovPartyMemberModel').modal('hide');
                            vc.emit('govPartyMemberManage', 'listGovPartyMember', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovPartyMemberModel: function () {
                $('#deleteGovPartyMemberModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
