(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovPartyOrgInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovPartyOrg', 'openDeleteGovPartyOrgModal', function (_params) {

                vc.component.deleteGovPartyOrgInfo = _params;
                $('#deleteGovPartyOrgModel').modal('show');

            });
        },
        methods: {
            deleteGovPartyOrg: function () {
                vc.http.apiPost(
                    '/govPartyOrg/deleteGovPartyOrg',
                    JSON.stringify(vc.component.deleteGovPartyOrgInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovPartyOrgModel').modal('hide');
                            vc.emit('govPartyOrgManage', 'listGovPartyOrg', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovPartyOrgModel: function () {
                $('#deleteGovPartyOrgModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
