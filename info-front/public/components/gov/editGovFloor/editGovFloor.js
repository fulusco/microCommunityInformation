(function (vc, vm) {

    vc.extends({
        data: {
            editGovFloorInfo: {
                govFloorId: '',
                caId: '',
                govCommunityId: '',
                floorNum: '',
                floorName: '',
                floorType: '',
                floorArea: '',
                layerCount: '',
                unitCount: '',
                floorUse: '',
                personName: '',
                personLink: '',
                floorIcon: '',
                oldFloorIcon: '',
                ramark: '',
                govCommunityAreas: [],
                govCommunitys: [] ,
                govBuildingTypes: []

            }
        },
        _initMethod: function () {
            $that._listEditFloorGovCommunityAreas();
            $that._listEditFloorGovBuildingTypes();
            $that._listEditFloorGovCommunitys($that.editGovFloorInfo.caId);
        },
        _initEvent: function () {
            vc.on('editGovFloor', 'openEditGovFloorModal', function (_params) {
                vc.component.refreshEditGovFloorInfo();
                $('#editGovFloorModel').modal('show');
                vc.copyObject(_params, vc.component.editGovFloorInfo);
                //vc.component.editGovFloorInfo.communityId = vc.getCurrentCommunity().communityId;
                let _tGovFloors = [];
                _tGovFloors.push(vc.component.editGovFloorInfo.floorIcon);
                vc.emit('editGovFloorCover','uploadImage', 'notifyPhotos',_tGovFloors);
            });
            vc.on("editGovFloor", "notifyUploadCoverImage", function (_param) {
                if(_param.length > 0){
                    vc.component.editGovFloorInfo.floorIcon = _param[0];
                }else{
                    vc.component.editGovFloorInfo.floorIcon = '';
                }
                
            });
        },
        methods: {
            editGovFloorValidate: function () {
                return vc.validate.validate({
                    editGovFloorInfo: vc.component.editGovFloorInfo
                }, {
                    'editGovFloorInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "建筑分类名称超长"
                        },
                    ],
                    'editGovFloorInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "小区名称超长"
                        },
                    ],
                    'editGovFloorInfo.floorNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "建筑物编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "建筑物编号超长"
                        },
                    ],
                    'editGovFloorInfo.floorName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "建筑物名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "建筑物名称超长"
                        },
                    ],
                    'editGovFloorInfo.floorType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "建筑物类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "建筑物类型超长"
                        },
                    ],
                    'editGovFloorInfo.floorArea': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "楼栋面积不能为空"
                        }
                    ],
                    'editGovFloorInfo.layerCount': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "楼层数不能为空"
                        }
                    ],
                    'editGovFloorInfo.unitCount': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "单元数不能为空"
                        }
                    ],
                    'editGovFloorInfo.floorUse': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "建筑物用途不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "建筑物用途超长"
                        },
                    ],
                    'editGovFloorInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "责任人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "责任人超长"
                        },
                    ],
                    'editGovFloorInfo.personLink': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "联系电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "联系电话格式错误"
                        },
                    ],
                    'editGovFloorInfo.floorIcon': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "建筑图片不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "建筑图片超长"
                        },
                    ],
                    'editGovFloorInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "描述不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "描述太长"
                        },
                    ],
                    'editGovFloorInfo.govFloorId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "建筑物ID不能为空"
                        }]

                });
            },
            editGovFloor: function () {
                if (!vc.component.editGovFloorValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govFloor/updateGovFloor',
                    JSON.stringify(vc.component.editGovFloorInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovFloorModel').modal('hide');
                            vc.emit('govFloorManage', 'listGovFloor', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditFloorGovCommunitys: function (_caId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.editGovFloorInfo.total = _govCommunityManageInfo.total;
                        vc.component.editGovFloorInfo.records = _govCommunityManageInfo.records;
                        vc.component.editGovFloorInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listEditFloorGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.editGovFloorInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.editGovFloorInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.editGovFloorInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listEditFloorGovBuildingTypes: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govBuildingType/queryGovBuildingType',
                    param,
                    function (json, res) {
                        var _govBuildingTypeManageInfo = JSON.parse(json);
                        vc.component.editGovFloorInfo.total = _govBuildingTypeManageInfo.total;
                        vc.component.editGovFloorInfo.records = _govBuildingTypeManageInfo.records;
                        vc.component.editGovFloorInfo.govBuildingTypes = _govBuildingTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            refreshEditGovFloorInfo: function () {
                let _govCommunityAreas = $that.editGovFloorInfo.govCommunityAreas;
                let _govCommunitys = $that.editGovFloorInfo.govCommunitys;
                let _govBuildingTypes = $that.editGovFloorInfo.govBuildingTypes;
                vc.component.editGovFloorInfo = {
                    govFloorId: '',
                    caId: '',
                    govCommunityId: '',
                    floorNum: '',
                    floorName: '',
                    floorType: '',
                    floorArea: '',
                    layerCount: '',
                    unitCount: '',
                    floorUse: '',
                    personName: '',
                    personLink: '',
                    floorIcon: '',
                    oldFloorIcon: '',
                    ramark: '',
                    govCommunityAreas: _govCommunityAreas,
                    govCommunitys: _govCommunitys ,
                    govBuildingTypes: _govBuildingTypes

                }
            }
        }
    });

})(window.vc, window.vc.component);
