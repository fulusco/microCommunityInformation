(function (vc) {
    vc.extends({
        propTypes: {
            emitChooseGovCompany: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovCompanyInfo: {
                govCompanys: [],
                _currentGovCompanyName: '',
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovCompany', 'openChooseGovCompanyModel', function (_param) {
                $('#chooseGovCompanyModel').modal('show');
                vc.component._refreshChooseGovCompanyInfo();
                vc.component._loadAllGovCompanyInfo(1, 10, '');
            });
        },
        methods: {
            _loadAllGovCompanyInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        communityId: vc.getCurrentCommunity().communityId,
                        name: _name
                    }
                };

                //发送get请求
                vc.http.apiGet('govCompany.listGovCompanys',
                    param,
                    function (json) {
                        var _govCompanyInfo = JSON.parse(json);
                        vc.component.chooseGovCompanyInfo.govCompanys = _govCompanyInfo.govCompanys;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovCompany: function (_govCompany) {
                if (_govCompany.hasOwnProperty('name')) {
                    _govCompany.govCompanyName = _govCompany.name;
                }
                vc.emit($props.emitChooseGovCompany, 'chooseGovCompany', _govCompany);
                vc.emit($props.emitLoadData, 'listGovCompanyData', {
                    govCompanyId: _govCompany.govCompanyId
                });
                $('#chooseGovCompanyModel').modal('hide');
            },
            queryGovCompanys: function () {
                vc.component._loadAllGovCompanyInfo(1, 10, vc.component.chooseGovCompanyInfo._currentGovCompanyName);
            },
            _refreshChooseGovCompanyInfo: function () {
                vc.component.chooseGovCompanyInfo._currentGovCompanyName = "";
            }
        }

    });
})(window.vc);
