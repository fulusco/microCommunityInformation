(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovPersonInfo: {
                govPersonId: '',
                caId: '',
                personType: '',
                idType: '',
                idCard: '',
                personName: '',
                personTel: '',
                personSex: '',
                prePersonName: '',
                birthday: '',
                nation: '',
                nativePlace: '',
                politicalOutlook: '',
                maritalStatus: '',
                religiousBelief: '',
                ramark: '',
                personTypeIs: 'addGovPerson',
                govCommunityAreas: []
            }
        },
        _initMethod: function () {
            vc.initDateTime('addBirthday', function (_value) {
                $that.addGovPersonInfo.birthday = _value;
            });
            $that._listAddGovCommunityAreas();
        },
        _initEvent: function () {
            vc.on('addGovPerson', 'openAddGovPersonModal', function (_param) {
                $('#addGovPersonModel').modal('show');
                $that.addGovPersonInfo.personType=_param._personType;
                if($that.addGovPersonInfo.personType){
                    $that.addGovPersonInfo.personTypeIs=_param._personType;
                };
            });
        },
        methods: {
            addGovPersonValidate() {
                return vc.validate.validate({
                    addGovPersonInfo: vc.component.addGovPersonInfo
                }, {
                    'addGovPersonInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属区域超长"
                        },
                    ],
                    'addGovPersonInfo.personType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "人口类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "人口类型超长"
                        },
                    ],
                    'addGovPersonInfo.idType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "证件类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "证件类型物超长"
                        },
                    ],
                    'addGovPersonInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "证件号码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "证件号码超长"
                        },
                    ],
                    'addGovPersonInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "姓名超长"
                        },
                    ],
                    'addGovPersonInfo.personTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "电话格式错误"
                        },
                    ],
                    'addGovPersonInfo.personSex': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "性别不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "性别超长"
                        },
                    ],
                    'addGovPersonInfo.prePersonName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "曾用名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "曾用名太长"
                        },
                    ],
                    'addGovPersonInfo.birthday': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "生日不能为空"
                        },
                        {
                            limit: "dateTime",
                            param: "",
                            errInfo: "不是有效的时间格式"
                        },
                    ],
                    'addGovPersonInfo.nation': [
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "民族超长"
                        },
                    ],
                    'addGovPersonInfo.nativePlace': [
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "籍贯超长"
                        },
                    ],
                    'addGovPersonInfo.politicalOutlook': [
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "政治面貌超长"
                        },
                    ],
                    'addGovPersonInfo.maritalStatus': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "婚姻状况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "婚姻状况超长了"
                        },
                    ],
                    'addGovPersonInfo.religiousBelief': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "宗教信仰不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "宗教信仰超长了"
                        },
                    ],
                    'addGovPersonInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],




                });
            },
            saveGovPersonInfo: function () {
                if (!vc.component.addGovPersonValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovPersonInfo);
                    $('#addGovPersonModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govPerson/saveGovPerson',
                    JSON.stringify(vc.component.addGovPersonInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovPersonModel').modal('hide');
                            vc.component.clearAddGovPersonInfo();
                            vc.emit('govPersonManage', 'listGovPerson', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _listAddGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.addGovPersonInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.addGovPersonInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.addGovPersonInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            clearAddGovPersonInfo: function () {
                vc.component.addGovPersonInfo = {
                    caId: '',
                    personType: '',
                    idType: '',
                    idCard: '',
                    personName: '',
                    personTel: '',
                    personSex: '',
                    prePersonName: '',
                    birthday: '',
                    nation: '',
                    nativePlace: '',
                    politicalOutlook: '',
                    maritalStatus: '',
                    religiousBelief: '',
                    ramark: '',
                    govCommunityAreas: []
                };
            }
        }
    });

})(window.vc);
