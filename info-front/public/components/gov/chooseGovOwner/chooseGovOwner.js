(function (vc) {
    vc.extends({
        propTypes: {
            emitChooseGovOwner: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovOwnerInfo: {
                govOwners: [],
                _ownerNum: '',
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovOwner', 'openChooseGovOwnerModel', function (_param) {
                $('#chooseGovOwnerModel').modal('show');
                vc.component._refreshChooseGovOwnerInfo();
                vc.component._loadAllGovOwnerInfo(1, 10, '');
            });
        },
        methods: {
            _loadAllGovOwnerInfo: function (_page, _row,_ownerNum) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        ownerNum: _ownerNum
                    }
                };

                //发送get请求
                vc.http.apiGet('/govOwner/queryGovOwner',
                    param,
                    function (json) {
                        var _govOwnerInfo = JSON.parse(json);
                        vc.component.chooseGovOwnerInfo.total = _govOwnerInfo.total;
                        vc.component.chooseGovOwnerInfo.records = _govOwnerInfo.records;
                        vc.component.chooseGovOwnerInfo.govOwners = _govOwnerInfo.data;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovOwner: function (_govOwner) {
                if (_govOwner.hasOwnProperty('name')) {
                    _govOwner.govOwnerName = _govOwner.name;
                }
                vc.emit($props.emitChooseGovOwner, 'chooseGovOwner', _govOwner);
                $('#chooseGovOwnerModel').modal('hide');
            },
            queryGovOwners: function () {
                vc.component._loadAllGovOwnerInfo(1, 10, vc.component.chooseGovOwnerInfo._ownerNum);
            },
            _refreshChooseGovOwnerInfo: function () {
                vc.component.chooseGovOwnerInfo._ownerNum = "";
            }
        }

    });
})(window.vc);
