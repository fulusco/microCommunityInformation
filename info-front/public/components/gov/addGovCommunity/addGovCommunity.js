(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovCommunityInfo: {
                govCommunityId: '',
                caId: '',
                communityName: '',
                propertyType: '',
                managerName: '',
                personName: '',
                personLink: '',
                communityIcon: '',
                communitySecure: '',
                ramark: '',
                govCommunityAreas: []

            }
        },
        _initMethod: function () {
            $that._listAddGovCommunityAreas();
        },
        _initEvent: function () {
            vc.on('addGovCommunity', 'openAddGovCommunityModal', function () {
                $('#addGovCommunityModel').modal('show');
            });
            vc.on("addGovCommunity", "notifyUploadCoverImage", function (_param) {
                if (_param.length > 0) {
                    console.log(_param);
                    vc.component.addGovCommunityInfo.communityIcon = _param[0];
                } else {
                    vc.component.addGovCommunityInfo.communityIcon = '';
                }
            });
        },
        methods: {
            addGovCommunityValidate() {
                return vc.validate.validate({
                    addGovCommunityInfo: vc.component.addGovCommunityInfo
                }, {
                    'addGovCommunityInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "建筑分类名称超长"
                        },
                    ],
                    'addGovCommunityInfo.communityName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "小区名称超长"
                        },
                    ],
                    'addGovCommunityInfo.propertyType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "物业状态不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "物业状态超长"
                        },
                    ],
                    'addGovCommunityInfo.managerName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "管理组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "管理组织名称超长"
                        },
                    ],
                    'addGovCommunityInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "责任人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "责任人超长"
                        },
                    ],
                    'addGovCommunityInfo.personLink': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "联系电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "30",
                            errInfo: "联系电话格式错误"
                        },
                    ],
                    'addGovCommunityInfo.communityIcon': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区图片不能为空"
                        }
                    ],
                    'addGovCommunityInfo.communitySecure': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区秘钥不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "小区秘钥超长"
                        },
                    ],
                    'addGovCommunityInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "描述不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "描述太长"
                        },
                    ],




                });
            },
            saveGovCommunityInfo: function () {
                if (!vc.component.addGovCommunityValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                //vc.component.addGovCommunityInfo.communityId = vc.getCurrentCommunity().communityId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovCommunityInfo);
                    $('#addGovCommunityModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govCommunity/saveGovCommunity',
                    JSON.stringify(vc.component.addGovCommunityInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovCommunityModel').modal('hide');
                            vc.component.clearAddGovCommunityInfo();
                            vc.emit('govCommunityManage', 'listGovCommunity', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _listAddGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.addGovCommunityInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.addGovCommunityInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.addGovCommunityInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            clearAddGovCommunityInfo: function () {
                vc.component.addGovCommunityInfo = {
                    caId: '',
                    communityName: '',
                    propertyType: '',
                    managerName: '',
                    personName: '',
                    personLink: '',
                    communityIcon: '',
                    communitySecure: '',
                    ramark: '',
                    govCommunityAreas: []
                };
            }
        }
    });

})(window.vc);
