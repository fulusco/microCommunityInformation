(function (vc, vm) {

    vc.extends({
        data: {
            editGovPersonInfo: {
                govPersonId: '',
                caId: '',
                personType: '',
                idType: '',
                idCard: '',
                personName: '',
                personTel: '',
                personSex: '',
                prePersonName: '',
                birthday: '',
                nation: '',
                nativePlace: '',
                politicalOutlook: '',
                maritalStatus: '',
                religiousBelief: '',
                ramark: '',
                govCommunityAreas: []
            }
        },
        _initMethod: function () {
            vc.initDateTime('editBirthday', function (_value) {
                $that.editGovPersonInfo.birthday = _value;
            });
            $that._listEditGovCommunityAreas();
        },
        _initEvent: function () {
            vc.on('editGovPerson', 'openEditGovPersonModal', function (_params) {
                vc.component.refreshEditGovPersonInfo();
                $('#editGovPersonModel').modal('show');
                vc.copyObject(_params, vc.component.editGovPersonInfo);
            });
        },
        methods: {
            editGovPersonValidate: function () {
                return vc.validate.validate({
                    editGovPersonInfo: vc.component.editGovPersonInfo
                }, {
                    'editGovPersonInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属区域超长"
                        },
                    ],
                    'editGovPersonInfo.personType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "人口类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "人口类型超长"
                        },
                    ],
                    'editGovPersonInfo.idType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "证件类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "证件类型物超长"
                        },
                    ],
                    'editGovPersonInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "证件号码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "证件号码超长"
                        },
                    ],
                    'editGovPersonInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "姓名超长"
                        },
                    ],
                    'editGovPersonInfo.personTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "电话格式错误"
                        },
                    ],
                    'editGovPersonInfo.personSex': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "性别不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "性别超长"
                        },
                    ],
                    'editGovPersonInfo.prePersonName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "曾用名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "曾用名太长"
                        },
                    ],
                    'editGovPersonInfo.birthday': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "生日不能为空"
                        },
                        {
                            limit: "dateTime",
                            param: "",
                            errInfo: "不是有效的时间格式"
                        },
                    ],
                    'editGovPersonInfo.nation': [
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "民族超长"
                        },
                    ],
                    'editGovPersonInfo.nativePlace': [
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "籍贯超长"
                        },
                    ],
                    'editGovPersonInfo.politicalOutlook': [
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "政治面貌超长"
                        },
                    ],
                    'editGovPersonInfo.maritalStatus': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "婚姻状况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "婚姻状况超长了"
                        },
                    ],
                    'editGovPersonInfo.religiousBelief': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "宗教信仰不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "宗教信仰超长了"
                        },
                    ],
                    'editGovPersonInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],
                    'editGovPersonInfo.govPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "人口管理ID不能为空"
                        }]

                });
            },
            editGovPerson: function () {
                if (!vc.component.editGovPersonValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govPerson/updateGovPerson',
                    JSON.stringify(vc.component.editGovPersonInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovPersonModel').modal('hide');
                            vc.emit('govPersonManage', 'listGovPerson', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.editGovPersonInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.editGovPersonInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.editGovPersonInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            refreshEditGovPersonInfo: function () {
                let _govCommunityAreas =  vc.component.editGovPersonInfo.govCommunityAreas;
                vc.component.editGovPersonInfo = {
                    govPersonId: '',
                    caId: '',
                    personType: '',
                    idType: '',
                    idCard: '',
                    personName: '',
                    personTel: '',
                    personSex: '',
                    prePersonName: '',
                    birthday: '',
                    nation: '',
                    nativePlace: '',
                    politicalOutlook: '',
                    maritalStatus: '',
                    religiousBelief: '',
                    ramark: '',
                    govCommunityAreas: _govCommunityAreas
                }
            }
        }
    });

})(window.vc, window.vc.component);
