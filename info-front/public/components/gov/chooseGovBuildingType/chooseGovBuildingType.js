(function (vc) {
    vc.extends({
        propTypes: {
            emitChooseGovBuildingType: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovBuildingTypeInfo: {
                govBuildingTypes: [],
                _currentGovBuildingTypeName: '',
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovBuildingType', 'openChooseGovBuildingTypeModel', function (_param) {
                $('#chooseGovBuildingTypeModel').modal('show');
                vc.component._refreshChooseGovBuildingTypeInfo();
                vc.component._loadAllGovBuildingTypeInfo(1, 10, '');
            });
        },
        methods: {
            _loadAllGovBuildingTypeInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        communityId: vc.getCurrentCommunity().communityId,
                        name: _name
                    }
                };

                //发送get请求
                vc.http.apiGet('govBuildingType.listGovBuildingTypes',
                    param,
                    function (json) {
                        var _govBuildingTypeInfo = JSON.parse(json);
                        vc.component.chooseGovBuildingTypeInfo.govBuildingTypes = _govBuildingTypeInfo.govBuildingTypes;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovBuildingType: function (_govBuildingType) {
                if (_govBuildingType.hasOwnProperty('name')) {
                    _govBuildingType.govBuildingTypeName = _govBuildingType.name;
                }
                vc.emit($props.emitChooseGovBuildingType, 'chooseGovBuildingType', _govBuildingType);
                vc.emit($props.emitLoadData, 'listGovBuildingTypeData', {
                    govBuildingTypeId: _govBuildingType.govBuildingTypeId
                });
                $('#chooseGovBuildingTypeModel').modal('hide');
            },
            queryGovBuildingTypes: function () {
                vc.component._loadAllGovBuildingTypeInfo(1, 10, vc.component.chooseGovBuildingTypeInfo._currentGovBuildingTypeName);
            },
            _refreshChooseGovBuildingTypeInfo: function () {
                vc.component.chooseGovBuildingTypeInfo._currentGovBuildingTypeName = "";
            }
        }

    });
})(window.vc);
