/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govOwnerManageInfo: {
                govOwners: [],
                total: 0,
                records: 1,
                moreCondition: false,
                govOwnerId: '',
                govCommunityAreas: [],
                conditions: {
                    caId: '',
                    ownerNum: '',
                    owneName: '',
                    ownerType: '',
                    idCard: '',
                    ownerTel: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovOwners(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._listGovCommunityAreas();
        },
        _initEvent: function () {

            vc.on('govOwnerManage', 'listGovOwner', function (_param) {
                vc.component._listGovOwners(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovOwners(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovOwners: function (_page, _rows) {

                vc.component.govOwnerManageInfo.conditions.page = _page;
                vc.component.govOwnerManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govOwnerManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govOwner/queryGovOwner',
                    param,
                    function (json, res) {
                        var _govOwnerManageInfo = JSON.parse(json);
                        vc.component.govOwnerManageInfo.total = _govOwnerManageInfo.total;
                        vc.component.govOwnerManageInfo.records = _govOwnerManageInfo.records;
                        vc.component.govOwnerManageInfo.govOwners = _govOwnerManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govOwnerManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.govOwnerManageInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.govOwnerManageInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.govOwnerManageInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovOwnerModal: function () {
                vc.emit('addGovOwner', 'openAddGovOwnerModal', {});
            },
            _openEditGovOwnerModel: function (_govOwner) {
                vc.emit('editGovOwner', 'openEditGovOwnerModal', _govOwner);
            },
            _openDeleteGovOwnerModel: function (_govOwner) {
                vc.emit('deleteGovOwner', 'openDeleteGovOwnerModal', _govOwner);
            },
            _queryGovOwnerMethod: function () {
                vc.component._listGovOwners(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govOwnerManageInfo.moreCondition) {
                    vc.component.govOwnerManageInfo.moreCondition = false;
                } else {
                    vc.component.govOwnerManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
