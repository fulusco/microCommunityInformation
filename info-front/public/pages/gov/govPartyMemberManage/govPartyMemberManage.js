/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govPartyMemberManageInfo: {
                govPartyMembers: [],
                total: 0,
                records: 1,
                moreCondition: false,
                govMemberId: '',
                componentShow: 'govPartyMemberManage',
                conditions: {
                    caId: '',
                    personId: '',
                    orgId: '',
                    govTypeId: '',
                    memberFlag: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovPartyMembers(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govPartyMemberManage', 'listGovPartyMember', function (_param) {
                $that.govPartyMemberManageInfo.componentShow ='govPartyMemberManage';
                vc.component._listGovPartyMembers(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovPartyMembers(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovPartyMembers: function (_page, _rows) {

                vc.component.govPartyMemberManageInfo.conditions.page = _page;
                vc.component.govPartyMemberManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govPartyMemberManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govPartyMember/queryGovPartyMember',
                    param,
                    function (json, res) {
                        var _govPartyMemberManageInfo = JSON.parse(json);
                        vc.component.govPartyMemberManageInfo.total = _govPartyMemberManageInfo.total;
                        vc.component.govPartyMemberManageInfo.records = _govPartyMemberManageInfo.records;
                        vc.component.govPartyMemberManageInfo.govPartyMembers = _govPartyMemberManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govPartyMemberManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovPartyMemberModal: function () {
                
                $that.govPartyMemberManageInfo.componentShow = 'addGovPartyMember';
            },
            _openEditGovPartyMemberModel: function (_govPartyMember) {

                $that.govPartyMemberManageInfo.componentShow = 'editGovPartyMember';
                vc.emit('editGovPartyMember', 'openEditGovPartyMemberModal', _govPartyMember);
            },
            _openDeleteGovPartyMemberModel: function (_govPartyMember) {
                vc.emit('deleteGovPartyMember', 'openDeleteGovPartyMemberModal', _govPartyMember);
            },
            _queryGovPartyMemberMethod: function () {
                vc.component._listGovPartyMembers(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govPartyMemberManageInfo.moreCondition) {
                    vc.component.govPartyMemberManageInfo.moreCondition = false;
                } else {
                    vc.component.govPartyMemberManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
